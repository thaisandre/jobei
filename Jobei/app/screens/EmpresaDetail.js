import React, { Component } from 'react';
import { ScrollView, StyleSheet, View, Text, Image, Button, AsyncStorage, Platform } from 'react-native';
import {Container, Content, Header, Body, Badge, List, ListItem, Thumbnail,} from 'native-base';

import {google} from '../config/data';
import {showToastTop, showToastBottom} from '../utils/helpers';
import {apiURI} from '../config/JobeiApi';
import axios from "axios";

const token = '';
class EmpresaDetail extends Component {

  constructor(props){
    super(props);

    this.state = {
      cnpj: '',
      nome: '',
      email: '',
      celular: '',
      descricao: '',
      endereco: '',
      imagem: '',
      valores: [],
    };
  }

  async componentDidMount() {
    await this.init();
    //console.warn(apiURI);
    //console.warn('ID: ' + this.props.navigation.state.params.idEmpresa);
    axios.get(apiURI + '/empresas/' + this.props.navigation.state.params.idEmpresa,
      {
        headers: {
        'Content-Type': 'application/json',
        'api-version': '1.0',
        'Authorization': 'Bearer ' + this.token
      }
    })
    .then(res => {
      let empresaApi = res.data
      this.setState({nome : empresaApi.nome});
      this.setState({cnpj: empresaApi.cnpj});
      this.setState({email: empresaApi.email});
      this.setState({celular: empresaApi.celular});
      this.setState({descricao: empresaApi.descricao});
      this.setState({endereco: empresaApi.endereco});
      this.setState({imagem: empresaApi.imagem});
      this.setState({valores: empresaApi.valores});
      //console.warn('ROLOOOOu');
    })
    .catch(error => {
      //console.warn('NAO ROLLOOOOU');
      showToastTop("Não foi possível carregar os dados. O servidor está em manutenção. Tente novamente mais tarde.");
      //console.warn(error.message);
    });
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
  }

  render() {
    //const { imagem, email, idEmpresa, nomeEmpresa, endereco } = this.props.navigation.state.params;

    const imagemApi = <Image style={styles.foto} source={{uri: this.state.imagem}} />;
    const padrao = <Image style={styles.foto} source={require('../resources/img/empresa_padrao.jpg')} />;

    return (
      <Content style={{backgroundColor: 'white'}}>
      <ScrollView>
        <List>
          <ListItem itemHeader first style={{ alignItems: "center" }}>


          <View>
            {Platform.OS === 'android' ?
              (<View style={{height: 90}}>{this.state.imagem == null ? padrao : imagemApi}</View>)
              :
              (<Text>{this.state.imagem == null ? padrao : imagemApi}</Text>)
            }
          </View>


          <View style={{ flexDirection: "column" }}>
            <Text style={{ fontWeight: "bold", fontSize: 16, marginLeft: 20 }}>
              {this.state.nome}
            </Text>
            <Text style={{ marginLeft: 20 }}>CNPJ: {this.state.cnpj}</Text>
          </View>
        </ListItem>
          
            <Text style={{ color: 'rgb(58, 203, 198)', fontSize:15 , marginLeft: 20, marginTop: 10}}>DESCRIÇÃO</Text>
          
          <ListItem>
            <Text>{this.state.descricao}</Text>
          </ListItem>
          
          <Text style={{ color: 'rgb(58, 203, 198)', fontSize: 13, marginLeft:20, marginTop: 15 }}>ENDEREÇO</Text>
          <ListItem>
            <Text>{this.state.endereco}</Text>
          </ListItem>
          <Text style={{ color: 'rgb(58, 203, 198)', fontSize: 13,  marginLeft:20, marginTop: 15 }}>EMAIL</Text>
          <ListItem>
            <Text>{this.state.email}</Text>
          </ListItem>
          <Text style={{ color: 'rgb(58, 203, 198)', fontSize: 13,  marginLeft:20, marginTop: 15 }}>TELEFONE</Text>
          <ListItem>
            <Text>{this.state.celular}</Text>
          </ListItem>
          
            <Text style={{ color: 'rgb(58, 203, 198)' , marginLeft: 20, marginTop: 15}}>VALORES</Text>
          
          <ListItem>
            <View style={{ flexDirection: "row" }}>
              <View>
                {this.state.valores.map(valor => (
                  <Badge
                    style={{
                      backgroundColor: 'rgb(58, 203, 198)',
                      borderBottomLeftRadius: 5,
                      borderBottomRightRadius: 5,
                      borderTopLeftRadius: 5,
                      borderTopRightRadius: 5,
                      marginTop: 5,
                      flexDirection: "row"
                    }}
                  >
                    <Text key={valor.id} style={{ color: "white" }}>{valor.nome}</Text>
                  </Badge>
                ))}
              </View>
            </View>
          </ListItem>
        </List>
      </ScrollView>
      </Content>
    );
  }
}

export default EmpresaDetail;

const styles = StyleSheet.create({
  container:{
    flex: 1,
    marginTop: 2,
    padding: 23,
  },
  principal: {
    alignItems: 'center',
  },
  cabecalho: {
    flexDirection: 'row',
  },
  dadosEmpresa: {
    width: 120,
  },
  dadosVaga: {
    marginTop: 50,
    width: '80%',
  },
  curtir: {
    marginTop: 10,
    alignItems: 'flex-end',
  },
  descricao: {
    marginTop: 30,
    marginBottom: 20,
    //alignItems: 'center',
  },
  valor: {
    marginLeft: 10,
  },
  titulo: {
    fontWeight: 'bold',
    marginTop: 10,
  },
  foto2: {
    resizeMode: 'contain',
    width: 80,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  foto: {
    width: 90,
    height: 90,
    marginTop: 5,
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
});
