import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image,
  TouchableOpacity, PixelRatio, AsyncStorage, ActivityIndicator, Modal} from 'react-native';
import {Container, Content, List, ListItem, Icon, Button,
  Thumbnail, Badge, Root} from 'native-base';

import axios from 'axios';
import { onSignIn } from "../config/auth";
import {apiURI, postCandidato} from '../config/JobeiApi';
import {showToastTop, showToastBottom} from '../utils/helpers';
import {imagem_padrao} from '../config/data';
import Loader from '../components/loader';


export default class FinalCadastro extends Component {

  constructor(props){
    super(props);

    this.state = {
      loading: false,
    }
  }

  async postCadastro(user){
    this.setState({loading: true});
    var habilidades = [];
    var valores = [];
    user.habilidades.forEach(x => habilidades.push({nome: x}));
    user.valores.forEach(x => valores.push({nome: x}));

    if(user.foto === null) {
      user.foto = imagem_padrao.foto;
    }

    user =
    {
      nome: user.nome,
      sobrenome: user.sobrenome,
      email: user.email,
      senha: user.senha,
      endereco: user.endereco,
      descricao: user.descricao,
      cargo: user.cargo,
      celular: user.celular,
      endereco: user.endereco,
      habilidades: habilidades,
      valores: valores,
      imagem: user.foto,
    }

    await postCandidato(user)
      .then(res => {
        this.setState({loading: false});
        this.props.navigation.navigate('Login');
      })
      .catch(error => {
        this.setState({loading: false});
      });
  }


  render() {

   const { foto, nome, sobrenome, email, senha, avatarSource, cargo, descricao, endereco, habilidades, valores, celular} = this.props.navigation.state.params;

   return(
     <Root>
    <Content style={{backgroundColor: 'white', padding: 20}}>
      <Loader loading={this.state.loading} />
      <View style={{alignItems: 'center', borderColor: 'rgb(0, 163, 151)', borderRadius: 5, borderWidth: 1,
               marginTop: 20, width: '90%'}}>
        <TouchableOpacity onPress = {() => this.postCadastro(this.props.navigation.state.params)}>
        <Text style={{fontSize: 12, padding: 10, height: 40, fontWeight: 'bold', color: 'rgb(0, 163, 151)',
                 textAlign: 'center', paddingBottom: 5}}>
                 CONCLUIR CADASTRO
        </Text>    
        </TouchableOpacity>
      </View>

      <View style={{alignItems: 'center', marginTop: 15}}>
        <Text>Confira seus dados antes de submetê-los:</Text>
      </View>


     <List>
       <ListItem itemHeader first style={{alignItems: 'center'}}>
        <View
          style={[
            styles.avatar,
            styles.avatarContainer,
            { marginBottom: 10 },
          ]}
          >
          {avatarSource === null ? (
            <Image style={styles.avatar} source={{uri: imagem_padrao.foto}}/>
          ) : (
            <Image style={styles.avatar} source={avatarSource} />
          )}
          </View>



         <View style={{flexDirection: 'column'}}>
         <Text style={{fontWeight: 'bold', fontSize: 16, marginLeft: 20}}>{nome + ' ' + sobrenome}</Text>
         <Text style={{marginLeft: 20}}>{cargo}</Text>
         </View>
       </ListItem>

       <ListItem itemHeader>
        <Text style={{color: 'rgb(58, 203, 198)'}}>DESCRIÇÃO</Text>
       </ListItem>

        <ListItem>
          <Text>{descricao}</Text>
        </ListItem>

       <ListItem itemHeader>
         <Text style={{color: 'rgb(58, 203, 198)'}}>DADOS</Text>
       </ListItem>

       <ListItem>
         <Text>{endereco}</Text>
       </ListItem>
       <ListItem>
         <Text>{celular}</Text>
       </ListItem>
       <ListItem>
         <Text>{email}</Text>
       </ListItem>

       <ListItem itemHeader>
         <View style={{flexDirection: 'row'}}>
         <Text style={{color: 'rgb(58, 203, 198)'}}>HABILIDADES</Text>
         <Text style={{color: 'rgb(58, 203, 198)', marginLeft: 60}}>VALORES</Text>
         </View>
       </ListItem>

       <ListItem>
         <View style={{flexDirection: 'row'}}>
           <View>
           {habilidades.map(habilidade =>
             <Badge
               style={{
                 backgroundColor: 'rgb(58, 203, 198)',
                 borderBottomLeftRadius: 5,
                 borderBottomRightRadius: 5,
                 borderTopLeftRadius: 5,
                 borderTopRightRadius: 5,
                 marginTop: 5,
                 flexDirection: 'row'}}>
                 <Text style={{color: 'white'}}>{habilidade}</Text>
             </Badge>
           )}
           </View>

           <View style={{marginLeft: 60}}>
             {valores.map(valor =>
             <Badge
               style={{
                 backgroundColor: 'rgb(58, 203, 198)',
                 borderBottomLeftRadius: 5,
                 borderBottomRightRadius: 5,
                 borderTopLeftRadius: 5,
                 borderTopRightRadius: 5,
                 marginTop: 5,
                 flexDirection: 'row'}}>
                 <Text style={{color: 'white'}}>{valor}</Text>
             </Badge>
           )}
           </View>
        </View>
       </ListItem>

     </List>
   </Content>
   </Root>
   );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 55,
    flexDirection: 'row',
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center'
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    height: 40,
    borderColor: '#8a8c91',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  botao: {
    backgroundColor: 'rgb(58, 203, 198)',
    marginTop: 20
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: 100,
    height:100,
  },
});
