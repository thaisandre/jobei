import React, { Component } from 'react';

import {Container, Content, Header, Body,
        List, ListItem, Thumbnail, Right, Left, Button, Icon} from 'native-base';

import {Platform, StyleSheet, Text, View, Alert,
        Image, AsyncStorage, ScrollView, TouchableOpacity} from 'react-native';

import Base64 from "../config/Base64";
import axios from "axios";
import {apiURI, getVagas, getCurtidas, getMatches} from '../config/JobeiApi';
import { messages } from '../config/data';

import {withNavigation, withNavigationFocus, NavigationEvents} from 'react-navigation';

const token = "";
const idCandidato = "";

class Atividades extends Component {

  constructor(props) {
    super(props);

    this.state = {
      curtidas: [],
      matches: [],
      vagas: [],
    }
  }

  async componentDidMount() {
    await this.pegarCurtidas();
    await this.pegarMatches();
    await this.pegarVagas();
  }

  async pegarCurtidas() {
    await this.init();
    getCurtidas(this.token, this.idCandidato)
      .then(res => {
        let curtidasApi = res;
        this.setState({curtidas: curtidasApi});
      })
      .catch(error => {
        showToastTop("Não foi possível carregar as curtidas. O servidor está em manutenção. Tente novamente mais tarde.");
      });
  }

  async pegarMatches() {
    await this.init();
    getMatches(this.token, this.idCandidato)
      .then(res => {
        let matchesApi = res;
        this.setState({matches: matchesApi});
      })
      .catch(error => {
        showToastTop("Não foi possível carregar as curtidas. O servidor está em manutenção. Tente novamente mais tarde.");
      });
  }

  async pegarVagas() {
    await this.init();
    getVagas(this.token)
      .then(res => {
        let vagasApi = res;
        this.setState({vagas : vagasApi});
      })
      .catch(error => {
        showToastTop(error.message);
      });
  }

  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
    //console.warn(this.parseJwt(this.token).sub);
  }

  goToVaga = (idVaga) => {
    var vaga = this.state.vagas.find(x => x.id === idVaga);
    this.props.navigation.navigate('VagaDetails', {... vaga});
  }

  render() {
    return (
      <Content style={{backgroundColor: 'white', padding: 20}}>
      <NavigationEvents
          onWillFocus={() => this.pegarVagas() && this.pegarMatches() && this.pegarCurtidas()}/>
        <View style={styles.container}>

        <View>
          <Text style={{fontSize: 16, color: 'rgb(58, 203, 198)'}}>CURTIDAS RECEBIDAS</Text>
            {this.state.curtidas.length === 0 ?
              (<Text style={{marginTop: 5, color: 'gray', marginLeft: 5}}>Você não possui nenhuma curtida até o momento</Text>)
              :
            (<ScrollView  horizontal={true} showsHorizontalScrollIndicator={false}>
                <View style={styles.curtidas}>
                {this.state.curtidas.map((curtida) => (
                  Platform.OS === 'android' ?
                    ( <TouchableOpacity onPress={() => this.goToVaga(curtida.vaga.id)}>
                        <Image source={{uri: curtida.vaga.caminhoFotoEmpresa}} style={styles.fotoAndroid} />
                      </TouchableOpacity>  )
                    :
                  ( <TouchableOpacity onPress={() => this.goToVaga(curtida.vaga.id)}>
                      <Thumbnail source={{uri: curtida.vaga.caminhoFotoEmpresa}} style={styles.foto} />
                    </TouchableOpacity> )

                ))}
              </View>
            </ScrollView>)
          }
        </View>

          <View style={styles.lineStyle}></View>

          <View style={{flexDirection: 'row', marginTop: 10}}>
            <Text style={{fontSize: 16, color: 'rgb(58, 203, 198)'}}>MATCHES</Text>
          </View>

          <View>
            {this.state.matches.length === 0?
              (<Text style={{marginTop: 5, color: 'gray', marginLeft: 5}}>Você não possui nenhum match até o momento</Text>)
              :

              (<List>
                {this.state.matches.map(matche =>
                <ListItem avatar key={matche.id}>
                  <TouchableOpacity onPress={() => this.goToVaga(matche.vaga.id)}>
                  <Left>
                      <Thumbnail small source={{uri: matche.vaga.caminhoFotoEmpresa}} />
                  </Left>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={() => Alert.alert('DEU UM MATCH! ','Entre em contato com a equipe de RH da empresa ' + matche.vaga.nomeEmpresa +  ' pelo telefone ' + matche.celular)
                  }>
                    <Body>
                      <Text style={{fontWeight: 'bold'}}>{matche.vaga.nomeEmpresa}</Text>
                      <Text note>{matche.vaga.cargo}</Text>
                    </Body>
                  </TouchableOpacity>
                  <Right>
                  </Right>
                </ListItem>
              )}
              </List>)
            }
          </View>
        </View>
    </Content>
    );
  }
}

export default withNavigationFocus(Atividades);

const styles = StyleSheet.create({
  container:{
    flex: 1,
    marginTop: 2,
    padding: 23,
  },
  principal: {
    alignItems: 'center',
  },
  curtidas: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  cabecalho: {
    flexDirection: 'row',
  },
  dadosEmpresa: {
    width: 120,
  },
  dadosVaga: {
    marginTop: 50
  },
  curtir: {
    marginTop: 10,
    alignItems: 'flex-end',
  },
  descricao: {
    marginTop: 30,
    marginBottom: 20,
    //alignItems: 'center',
  },
  valor: {
    marginLeft: 10,
  },
  titulo: {
    fontWeight: 'bold',
    marginTop: 10,
  },
  foto2: {
    resizeMode: 'contain',
    width: 100,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  foto: {
    resizeMode: 'contain',
    width: 60,
    height: 60,
    marginTop: 5,
    marginLeft: 15,
    borderWidth: 1,
    borderColor: 'gray',
  },
  fotoAndroid: {
    //resizeMode: 'contain',
    width: 60,
    height: 60,
    borderRadius: 30,
    marginTop: 5,
    marginLeft: 20,
    borderWidth: 1,
    borderColor: 'gray',
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
});
