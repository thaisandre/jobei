import React, { Component } from 'react';
import { ScrollView, StyleSheet, View, Text, Image,
  Button, TouchableOpacity, AsyncStorage, BackHandler, Platform } from 'react-native';

import { Tile, List, ListItem } from 'react-native-elements';
import {Container, Content, Header, Body, Badge, Root} from 'native-base';

import {withNavigation, withNavigationFocus} from 'react-navigation';

import axios from 'axios';
import {apiURI, curtir, descurtir} from '../config/JobeiApi';
import Base64 from "../config/Base64";
import {showToastBottom, showToastTop} from '../utils/helpers';

const token = "";
const idCandidato = "";

class VagaDetail extends Component {

  constructor(props) {
    super(props);

    this.state = {
      curtiu: this.props.navigation.state.params.curtiu,
    };
   }

  goToScreen = (vaga) => {
    this.props.navigation.navigate('EmpresaDetails', { ...vaga });
  };

  async curtir(vaga) {
    await this.init();

    if(this.state.curtiu === false){
      var curtida = curtir(vaga, this.token, this.idCandidato)
        .then(res => {
          this.setState({curtiu: true})
        })
        .catch(error => {
          this.setState({curtiu: false});
        });
    } else {
      var descurtida = descurtir(vaga, this.token)
      .then(res => {
        this.setState({curtiu: false});
      })
      .catch(error => {
        this.setState({curtiu: true});
      });
    }
  }

  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  };

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
  };

  render() {
    const { id, imagem, cargo, email, descricao, porcentagem, idEmpresa, nomeEmpresa, endereco, remuneracao, habilidades, curtiu } = this.props.navigation.state.params;
    const curtida = <Image source={require('./../resources/img/s2-checked.png')}
      style={{width: 35, height: 35}} />
    const naoCurtida = <Image source={require('./../resources/img/s2.png')}
      style={{width: 35, height: 35}} />

    //const imagemEmpresaApi = <Thumbnail large source={{uri: this.state.foto}} />;
    //const padrao = <Thumbnail large source={require('../resources/img/empresa_padrao.jpg')} />;
    var img = imagem;
    return (

      <Root>
      <Content style={{ backgroundColor: "white" }}>
      <ScrollView>
        <View style={styles.container}>

            <View style={styles.cabecalho}>
              <View style={styles.dadosEmpresa}>
                <Text style={{fontWeight: 'bold', marginTop: 10, fontSize: 20}}>
                  {cargo}</Text>

                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity onPress={() => this.goToScreen(this.props.navigation.state.params)}>
                      <Image style={styles.foto2} source={{uri: img}}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.goToScreen(this.props.navigation.state.params)}>
                      <Text style={styles.nomeEmpresa}>{nomeEmpresa}</Text>
                    </TouchableOpacity>
                  </View>
              </View>

              <View style={styles.curtir}>
                {Platform.OS === 'android' ?
                ( <TouchableOpacity onPress={() => this.curtir(this.props.navigation.state.params)}>
                  <View style={{height: 35}}>{this.state.curtiu? curtida : naoCurtida}</View>
                </TouchableOpacity> )
                :
                ( <TouchableOpacity onPress={() => this.curtir(this.props.navigation.state.params)}>
                  <Text>{this.state.curtiu? curtida : naoCurtida}</Text>
                </TouchableOpacity> )
              }
              </View>
            </View>

            <View style={styles.lineStyle} />

            <View>
              <Text style={styles.titulo}>Descrição</Text>
              <Text style={styles.valor}>{descricao}</Text>
              <Text style={styles.titulo}>Salário</Text>
              <Text style={styles.valor}>{remuneracao}</Text>

              <Text style={styles.titulo}>Horário</Text>
              <Text style={styles.valor}>A combinar</Text>
            </View>

            <View>
              <Text style={styles.titulo}>Habilidades</Text>
              {habilidades.map(habilidade =>
                <View style={{marginLeft: 5, flexDirection: 'row'}}>
                <Badge
                  style={{
                    backgroundColor: 'rgb(58, 203, 198)',
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderTopRightRadius: 5,
                    marginTop: 5,
                    flexDirection: 'row'}}>
                    <Text style={{color: 'white'}}>{habilidade.nome}</Text>
                </Badge>
                </View>
              )}
            </View>
          </View>
      </ScrollView>
      </Content>
      </Root>
    );
  }
}

export default withNavigationFocus(VagaDetail);

const styles = StyleSheet.create({
  container:{
    flex: 1,
    marginTop: 2,
    padding: 23,
  },
  principal: {
    alignItems: 'center',
  },
  cabecalho: {
    flexDirection: 'row',
  },
  dadosEmpresa: {
    width: 120,
    width: '80%',
    alignItems: 'flex-start',
  },
  dadosVaga: {
    marginTop: 50
  },
  curtir: {
    marginTop: 10,
    width: '20%',
    alignItems: 'flex-end',
  },
  descricao: {
    marginTop: 30,
    marginBottom: 20,
    //alignItems: 'center',
  },
  valor: {
    marginLeft: 10,
    marginBottom: '5%',
  },
  nomeEmpresa: {
    marginLeft: 10,
    marginTop: '35%',
    color: 'rgb(58, 203, 198)',
    textDecorationLine: 'underline',
  },
  titulo: {
    fontWeight: 'bold',
    marginTop: 10,
  },
  foto2: {
    resizeMode: 'contain',
    width: 80,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  foto: {
    width: 90,
    height: 90,
    marginTop: 5,
  },
  foto2: {
    //marginLeft: 10,
    width: 40,
    height: 40,
  },
  lineStyle: {
    marginTop: 15,
    marginBottom: 15,
    borderBottomColor: 'rgb(58, 203, 198)',
    borderBottomWidth: 1
  },
});
