import React, { Component } from 'react';

import {Text, View, ScrollView, StyleSheet, Image,
  Dimensions, Button, AsyncStorage, TouchableOpacity, Platform} from 'react-native';

import { List, ListItem, Card, Icon , Avatar} from 'react-native-elements';
import {Container, Content} from 'native-base';
import {withNavigation, withNavigationFocus, NavigationEvents} from 'react-navigation';

import { vagas } from '../config/data';
import {apiURI, getVagas} from '../config/JobeiApi';

import axios from 'axios';
const token = '';

import PropTypes from 'prop-types';

class Home extends Component {

  constructor(props) {
    super(props)

    this.state = {
      vagas: [],
    }

  }

  async componentDidMount() {
    await this.pegarVagas();
  }

  async pegarVagas() {
    await this.init();
    getVagas(this.token)
      .then(res => {
        let vagasApi = res;
        this.setState({vagas : vagasApi});
      })
      .catch(error => {
        showToastTop('não foi possível editar o(s) dado(s)');
      });
  }

  async init() {
      this.token = await AsyncStorage.getItem('token');
  }

  goToScreen = (vaga) => {
    this.props.navigation.navigate('VagaDetails', { ...vaga});
  };

  render() {
    const curtida = <Image source={require('./../resources/img/s2-checked.png')}
      style={styles.curtir} />
    const naoCurtida = <Image source={require('./../resources/img/s2.png')}
      style={styles.curtir} />

    var vagasOrdenadas = this.state.vagas.sort(function(a, b){
        return a.porcentagem > b.porcentagem ? -1 : a.porcentagem < b.porcentagem ? 1 : 0;
      });

    return (

      <Content style={{backgroundColor: 'white'}}>

          <NavigationEvents
              onWillFocus={() => this.pegarVagas()}/>

          <List>
          {vagasOrdenadas.map((vaga) => (
            <View style={{alignItems: 'center'}}>
            <TouchableOpacity onPress={() => this.goToScreen(vaga)}>
              <Card
                containerStyle={{width: Dimensions.get('window').width - 100}}
                >
                <View>

                  <View style={{alignItems: 'flex-start'}}>
                      <Text>{vaga.nomeEmpresa}</Text>
                  </View>


                  <View style={styles.imgContainer}>
                    <Image
                        style={styles.foto}
                        source={{uri: vaga.imagem}}
                        />
                  </View>


                  <View style={styles.dados}>
                      <Text style={styles.descricao}>
                        {vaga.cargo}
                      </Text>
                      <Text style={styles.porcentagem}>
                         {vaga.porcentagem}
                      </Text>
                  </View>

                  <View style={{alignItems: 'flex-start'}}>
                    <View style={{margintTop: 20}}>
                      <Text>
                          {vaga.descricao}
                        </Text>
                    </View>
                  </View>
              </View>

              {Platform.OS == 'ios' ?
                <View style={{alignItems: 'flex-end'}}>
                  <View>{vaga.curtiu ? curtida : naoCurtida}</View>
                </View>
              :
               <View style={{alignItems: 'flex-end', height: 20}}>
                <View style={{height:25}}>{vaga.curtiu? curtida : naoCurtida}</View>
              </View> }
            </Card>
            </TouchableOpacity>
          </View>
        ))}
        </List>
      </Content>
    );
  }
}

export default withNavigationFocus(Home);

const styles = StyleSheet.create({
  container:{
    alignItems: 'center'
  },
  imgContainer:{
    alignItems: 'center',
    marginTop: 15,
    marginBottom: 15
  },
  foto: {
    height: 60,
    width: 60,
    borderRadius: 20,
  },
  dados: {
    alignItems: 'flex-start',
    flexDirection: 'row',
    width: 225
  },
  descricao:{
    alignItems: 'flex-start',
    marginBottom: 10,
    fontWeight: 'bold'
  },
  porcentagem:{
    marginLeft: 10,
    color: 'rgb(0, 163, 151)',
    fontWeight: 'bold'
  },
  curtir: {
    resizeMode: 'contain',
    marginTop: 5,
    marginLeft: 12,
    width: 20,
    height: 20,
  },
});
