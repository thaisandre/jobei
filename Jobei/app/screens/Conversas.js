import React, { Component } from 'react';

import {Container, Text, Content, ListItem, List, Header, Body, Right, Left, Thumbnail} from 'native-base';
import {Icon} from 'native-base';
import {Image, StyleSheet, View} from 'react-native';

import { conversas } from '../config/data';

export default class Conversas extends Component {

  render(){

    const messages = conversas.messages.sort(function(a, b){
      return a.time < b.time ? -1 : a.time > b.time ? 1 : 0;
    });

    return(
      <Content style={{padding: 23, backgroundColor: 'white'}}>
        <List>
          {messages.map(message => (
            <ListItem avatar key={message.id}>
              <Left>
                  <Thumbnail small source={require('./../resources/img/sam.jpeg')} />
              </Left>
              <Body>
                <Text>{message.valor}</Text>
                <Text note>{message.time}</Text>
              </Body>
            </ListItem>
          ))}
          </List>
      </Content>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    marginTop: 2,
    padding: 23,
  },
  principal: {
    alignItems: 'center',
  },
  foto2: {
    resizeMode: 'contain',
    width: 100,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: 'gray',
    borderBottomWidth: 1
  },
});
