import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ScrollView, Image,
  TextInput, AsyncStorage, TouchableOpacity, PixelRatio,
  Alert, ActivityIndicator, Modal} from 'react-native';

import { Container, Content, Header, Body, Toast, Root, Picker,
  Form, Icon, Textarea, Button } from 'native-base';
import { TabNavigator, StackNavigator, createSwitchNavigator } from 'react-navigation';
import { habilidades, cargos, imagem_padrao } from '../config/data';
import { onSignIn } from "../config/auth";
import AppIntroSlider from 'react-native-app-intro-slider';
import ImagePicker from 'react-native-image-picker';
import {showToastBottom, showToastTop} from '../utils/helpers';
import Loader from '../components/loader';
import Base64 from "../config/Base64";

import axios from "axios";
import {apiURI, updateCandidato} from '../config/JobeiApi';

const token = "";
const idCandidato = "";

export default class EditarFoto extends Component {

  constructor(props) {
    super(props);

    this.state = {
      avatarSource: null,
      foto: null,
      loading: false,
    }

    this.selectedPhotoTapped = this.selectedPhotoTapped.bind(this);
  }

  selectedPhotoTapped() {
    const options = {
      quality: 1.0,
      maxWidth: 90,
      maxHeight: 90,
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      //console.warn('Response = ', response);

      if (response.didCancel) {
        //console.warn('User cancelled image picker');
      } else if (response.error) {
        //console.warn('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        //console.warn('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };
        this.setState({ foto: 'data:image/jpeg;base64,' + response.data });
        this.setState({
          avatarSource: source,
        });
      }
    });
  }

  async componentDidMount() {
    await this.init();
    let perfilApi = null;


    axios.get( apiURI + '/candidatos/' + this.idCandidato,
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + this.token
    }})
    .then(res => {
      perfilApi = res.data;
      let source = {uri: perfilApi.imagem + '?random_number=' + new Date().getTime()};
      this.setState({foto: perfilApi.imagem});
      this.setState({avatarSource: source});
    })
    .catch(error => {
      this.showToast('Não foi possível carregar os dados do usuário. Tente novamente mais tarde');
    });
  }


  async alterarCandidato() {
    this.setState({loading: true});
    await this.init();

    var habs = [];
    this.props.navigation.state.params.habilidades.forEach(x => habs.push({nome: x.nome}));

    var vals = [];
    valores: this.props.navigation.state.params.valores.forEach(x => vals.push({nome: x.nome}));

    //console.warn('habs ' + JSON.stringify(habs));
    //console.warn('vals ' + JSON.stringify(vals));
    var user = {
      nome: this.props.navigation.state.params.nome,
      sobrenome: this.props.navigation.state.params.sobrenome,
      email: this.props.navigation.state.params.email,
      celular: this.props.navigation.state.params.celular,
      cargo: this.props.navigation.state.params.cargo.nome,
      endereco: this.props.navigation.state.params.endereco,
      descricao: this.props.navigation.state.params.descricao,
      habilidades: habs,
      valores: vals,
      imagem64: this.state.foto,
      avatarSource: this.state.avatarSource,
    }

    await updateCandidato(user, this.token, this.idCandidato)
        .then(res => {
          this.setState({loading: false});
          //showToastTop('foto editada com sucesso');
          this.props.navigation.navigate('Perfil');
        })
        .catch(error => {
          this.setState({loading: false});
          showToastTop('não foi possível alterar a foto');
        });
    }


  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
  }

  goToScreen = (screenName) => {
    this.props.navigation.navigate(screenName);
  }

  getImage() {
    var url = this.state.foto;
    url = url + '?random_number=' + new Date().getTime();
    let source = {uri: url};
    this.setState({avatarSource: source});
  }

  render() {


    return (
      //<Content style={{backgroundColor: 'white'}}>
    <Root>
      <View style={styles.container}>

        <Loader loading={this.state.loading} />

        <View style={styles.titulo}>
          <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Foto</Text>
        </View>

        <View style={styles.lineStyle} />

        <View style={{ marginTop: '10%', marginBottom: '5%', alignItems: 'center' }}>
          <TouchableOpacity onPress={this.selectedPhotoTapped.bind(this)}>
            <View
              style={[
                styles.avatar,
                styles.avatarContainer,
                { marginBottom: 10 },
              ]}
            >
              {this.state.avatarSource === null ? (
                <Text style={{ fontSize: 12 }}>Selecione a Foto</Text>
              ) : (
                  <Image style={styles.avatar} source={this.state.avatarSource} />
                )}
            </View>
          </TouchableOpacity>
        </View>

        <View style={{alignItems: 'center'}}>
          <Text style={{marginTop: 20}}>clique na foto para editar</Text>

          <TouchableOpacity onPress={() => this.alterarCandidato()}>
            <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: 'rgb(0, 163, 151)' }}>EDITAR</Text>
          </TouchableOpacity>
          </View>
      </View>

    </Root>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    backgroundColor: 'white',
  },
  lineStyle: {
    marginTop: '3%',
    borderBottomColor: 'rgb(58, 203, 198)',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center',
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: 'center',
    height: 40,
    //borderColor: '#8a8c91',
    borderColor: '#9B9B9B',
    //borderColor: 'rgb(58, 203, 198)',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  botao: {
    backgroundColor: 'rgb(58, 203, 198)',
    marginTop: 20
  },
  titulo: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  erroBorder: {
    borderWidth: 1,
    borderColor: 'red',
  },
  erroText: {
    color: 'red',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});
