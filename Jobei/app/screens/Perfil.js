import React, { Component } from "react";

import { Container, Content, Header, Body, List, ListItem,
  Thumbnail, Badge, Root} from "native-base";

import { Platform, StyleSheet, Text, View, Button,
  Image, AsyncStorage, Dimensions} from "react-native";

import { me } from "../config/data";

import Base64 from "../config/Base64";
import {withNavigation, withNavigationFocus, NavigationEvents} from 'react-navigation';

import axios from "axios";
import {apiURI, getCandidato} from '../config/JobeiApi';
import {showToastBottom, showToastTop} from '../utils/helpers';

const token = "";
const idCandidato = "";


class Perfil extends Component {

  constructor(props) {
    super(props);

    this.state = {
      nome: '',
      sobrenome: '',
      email: '',
      descricao: '',
      celular: '',
      cargo: [],
      habilidades: [],
      valores: [],
      imagem: '',
      window: Dimensions.get('window'),
    }
  }

  async componentDidMount() {
    await this.pegarCandidato();
  }

  async pegarCandidato() {
    await this.init();
    getCandidato(this.token, this.idCandidato)
    .then(res => {
      let perfilApi = res;
      this.setState({nome: perfilApi.nome});
      this.setState({sobrenome: perfilApi.sobrenome});
      this.setState({endereco: perfilApi.endereco});
      this.setState({email: perfilApi.email});
      this.setState({celular: perfilApi.celular});
      this.setState({descricao: perfilApi.descricao});
      this.setState({cargo: perfilApi.cargo});
      this.setState({habilidades: perfilApi.habilidades});
      this.setState({valores: perfilApi.valores});
      this.setState({imagem: perfilApi.imagem});
    })
    .catch(error => {
      showToastTop('Não foi possível carregar o Perfil. Tente novamente mais tarde');
    });
  }

  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
  }

  getImage() {
    var url = this.state.imagem;
    url = url + '?random_number=' + new Date().getTime();
    return url;
  }

  render() {
    const screenWidth = Dimensions.get('window').width;

    const valoresOrdenados = this.state.valores.sort(function(a, b){
        return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
    });

    const habilidadesOrdenadas = this.state.habilidades.sort(function(a, b){
        return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
    });

    var url = this.getImage();
    var imagemApi = <Thumbnail large source={{uri: url}} />;
    const padrao = <Thumbnail large source={require('../resources/img/padrao.jpg')} />;
    var imagemApiAndroid = <Image style={{width: 90, height: 90, borderRadius: 45}} source={{uri: url}} />;
    const padraoAndroid = <Image style={{width: 90, height: 90, borderRadius: 45}} source={require('../resources/img/padrao.jpg')} />;

    return (
      <Root>
      <Content style={{ backgroundColor: "white" }}>

      <NavigationEvents
          onWillFocus={() => this.pegarCandidato()}/>

        <List>
          <ListItem itemHeader first style={{ alignItems: "center" }}>

            <View>
            {Platform.OS === 'android' ?
              ( <View>{this.state.imagem === null ? padraoAndroid : imagemApiAndroid}</View> ) :
              ( <Text>{this.state.imagem === null ? padrao : imagemApi}</Text> )
            }
            </View>

            <View style={{ flexDirection: "column" }}>
              <Text
                style={{ fontWeight: "bold", fontSize: 16, marginLeft: 20 }}
              >
                {this.state.nome + " " + this.state.sobrenome}
              </Text>
              <Text key={this.state.cargo.id} style={{ marginLeft: 20 }}>{this.state.cargo.nome}</Text>
            </View>
          </ListItem>
            <Text style={{ color: 'rgb(58, 203, 198)', fontSize:15 , marginLeft: 20, marginTop: 10}}>DESCRIÇÃO</Text>
          <ListItem>
            <Text>{this.state.descricao}</Text>
          </ListItem>


           <Text style={{ color: 'rgb(58, 203, 198)', fontSize: 13, marginLeft:20, marginTop: 15 }}>ENDEREÇO</Text>
          <ListItem>
            <Text>{this.state.endereco}</Text>
          </ListItem>
          <Text style={{ color: 'rgb(58, 203, 198)', fontSize: 13, marginLeft:20, marginTop: 15 }}>EMAIL</Text>
          <ListItem>
            <Text>{this.state.celular}</Text>
          </ListItem>
          <Text style={{ color: 'rgb(58, 203, 198)', fontSize: 13, marginLeft:20, marginTop: 15 }}>TELEFONE</Text>
          <ListItem>
            <Text>{this.state.email}</Text>
          </ListItem>
            <Text style={{ color: 'rgb(58, 203, 198)', marginLeft: 20, marginTop: 15 }}>HABILIDADES</Text>

          <ListItem>
            <View style={{flexDirection: 'row', flexWrap: 'wrap', width: screenWidth - 20}}>
              {habilidadesOrdenadas.map(habilidade => (
                <Badge
                  style={{
                    backgroundColor: 'rgb(58, 203, 198)',
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderTopRightRadius: 5,
                    marginTop: 5,
                    marginRight: 5,

                  }}
                >
                  <Text key={habilidade.id} style={{ color: "white" }}>{habilidade.nome}</Text>
                </Badge>
              ))}
            </View>
          </ListItem>

            <Text style={{ color: 'rgb(58, 203, 198)', marginLeft: 20, marginTop: 15 }}>VALORES</Text>

          <ListItem>
            <View style={{flexDirection: 'row', flexWrap: 'wrap', width: screenWidth - 20}}>
              {valoresOrdenados.map(valor => (
                <Badge
                  style={{
                    backgroundColor: 'rgb(58, 203, 198)',
                    borderBottomLeftRadius: 5,
                    borderBottomRightRadius: 5,
                    borderTopLeftRadius: 5,
                    borderTopRightRadius: 5,
                    marginTop: 5,
                    marginRight: 5,
                  }}
                >
                  <Text key={valor.id} style={{ color: "white" }}>{valor.nome}</Text>
                </Badge>
              ))}
          </View>
        </ListItem>

        </List>
      </Content>
      </Root>
    );
  }
}

export default withNavigationFocus(Perfil);

const styles = StyleSheet.create({
  container: {
    padding: 55,
    flexDirection: "row"
  },
  lineStyle: {
    marginTop: 20,
    borderBottomColor: "gray",
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: "center"
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: "center",
    height: 40,
    borderColor: "#8a8c91",
    backgroundColor: "#ffffff",
    borderWidth: 1,
    width: "90%",
    borderRadius: 5
  },
  botao: {
    backgroundColor: 'rgb(58, 203, 198)',
    marginTop: 20
  }
});

Perfil.defaultProps = { ...me };
