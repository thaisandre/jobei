import React, { Component } from 'react';

import {Platform, StyleSheet, Text, View, ScrollView, Image,
  TextInput, AsyncStorage, Button, TouchableOpacity,
  ActivityIndicator, Modal} from 'react-native';

import {Container, Content, Header, Body, Toast, Root, Picker,
  Form, Icon, Badge} from 'native-base';

import {withNavigation} from 'react-navigation';

import axios from 'axios';
import Loader from '../components/loader';
//import {getEnderecoApi} from '../config/restApi';
//import { habilidades } from '../config/data';
import { onSignIn } from "../config/auth";
import MultiSelect from 'react-native-multiple-select';
import {apiURI, updateCandidato, getValores} from '../config/JobeiApi';
import {showToastTop, showToastBottom} from '../utils/helpers';
import Base64 from "../config/Base64";

const token = "";
const idCandidato = "";

export default class EditarValores extends Component {

  constructor(props){
    super(props);

    this.state = {
      nome: this.props.navigation.state.params.nome,
      sobrenome: this.props.navigation.state.params.sobrenome,
      email: this.props.navigation.state.params.email,
      celular: this.props.navigation.state.params.celular,
      descricao: this.props.navigation.state.params.descricao,
      endereco: this.props.navigation.state.params.endereco,
      valores: this.props.navigation.state.params.valores,
      cargo: this.props.navigation.state.params.cargo,
      imagem: this.props.navigation.state.params.imagem,
      imagem64: this.props.navigation.state.params.imagem64,
      habilidades: this.props.navigation.state.params.habilidades,
      valoresApi: [],
      selectedItems: [],
      loading: false,
    }
  }

  onSelectedItemsChange = (selectedItems) => {
    //console.warn('x: ' + selectedItems);
   this.setState({ selectedItems });
  };

  async componentDidMount() {
    await this.init();
    var items = [];
    this.state.valores.forEach(x => items.push(x.nome));
    this.setState({selectedItems: items});
    this.pegarValores();
  }

  pegarValores() {
    getValores()
    .then(res => {
      this.setState({valoresApi: res});
    })
    .catch(error => {
      showToastTop('Não foi possível carregar os valores. O servidor está em manutenção. Tente novamente mais tarde.');
    });
  }

  async alterarCandidato(){

    if(this.state.selectedItems.length < 5){
      showToastTop('Escolha 5 valores');
    } else {

      this.setState({loading: true});
      var habs = [];
      this.state.habilidades.forEach(x => habs.push({nome: x.nome}));


      var vals = [];
      this.state.selectedItems.forEach(x => vals.push({nome: x}));
      await this.init();
      var user = {
        nome: this.props.navigation.state.params.nome,
        sobrenome: this.props.navigation.state.params.sobrenome,
        email: this.props.navigation.state.params.email,
        celular: this.props.navigation.state.params.celular,
        cargo: this.props.navigation.state.params.cargo.nome,
        endereco: this.props.navigation.state.params.endereco,
        descricao: this.props.navigation.state.params.descricao,
        habilidades: habs,
        valores: vals,
        imagem: this.props.navigation.state.params.imagem,
        imagem64: this.props.navigation.state.params.imagem64,
      }

      //console.warn(user.imagem64);

      await updateCandidato(user, this.token, this.idCandidato)
        .then(res => {
          this.setState({loading: false});
          //showToastTop('Valores editados com sucesso');
          this.props.navigation.navigate('Perfil');
        })
        .catch(error => {
          this.setState({loading: false});
          showToastTop('Não foi possível editar os valores');
        });
    }
  }

  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
  }

 render(){
   const { selectedItems } = this.state;
   if(this.state.selectedItems.length > 5){
     this.state.selectedItems.pop();
     alert('Você pode escolher apenas 5 valores');
   }


   return(
     <Root>
     <Content style={{backgroundColor: 'white', padding: 40}}>

        <Loader loading={this.state.loading} />

        <View style={{flexDirection: 'row'}}>
          <Text style={{fontSize: 16, fontWeight: 'bold',
              alignItems: 'flex-start'}}>Valores</Text>
        </View>

        <View style={styles.lineStyle} />

         <View style={{ flex: 1}}>


           <View style={{marginTop: 30}}>
              <MultiSelect
                items={this.state.valoresApi}
                uniqueKey="nome"
                ref={(component) => { this.multiSelect = component }}
                onSelectedItemsChange={this.onSelectedItemsChange}
                selectedItems={selectedItems}
                selectText="Selecione 5 valores"
                searchInputPlaceholderText="Search items..."
                onChangeInput={ (text)=> console.log(text)}
                tagRemoveIconColor="#CCC"
                tagBorderColor="rgb(58, 203, 198)"
                tagTextColor="rgb(58, 203, 198)"
                selectedItemTextColor="rgb(58, 203, 198)"
                selectedItemIconColor="rgb(58, 203, 198)"
                itemTextColor="#000"
                displayKey="nome"
                searchInputStyle={{ color: '#CCC' }}
                submitButtonColor="rgb(58, 203, 198)"
                submitButtonText="confirmar"
              />
              </View>
          </View>

          <View style={styles.inputContainer2}>
             <TouchableOpacity onPress = {() => this.alterarCandidato()}>
               <View style={{borderColor: 'rgb(0, 163, 151)', borderRadius: 5, borderWidth: 1,
               marginTop: 20, width: '90%'}}>
                 <Text style={{fontSize: 12, padding: 10, height: 40, fontWeight: 'bold', color: 'rgb(0, 163, 151)',
                 textAlign: 'center', paddingBottom: 5}}>
                   EDITAR
                 </Text>
               </View>
             </TouchableOpacity>
           </View>
    </Content>
    </Root>
   );
 }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //justifyContent: 'center',
    //alignItems: 'center',
    padding: 40,
  },
  lineStyle: {
    marginBottom: '3%',
    marginTop: '3%',
    borderBottomColor: 'rgb(58, 203, 198)',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center'
  },
  inputContainer2: {
    alignItems: 'center',
    marginTop: 40
  },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    height: 40,
    borderColor: '#8a8c91',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  botao: {
    backgroundColor: 'rgb(58, 203, 198)',
    marginTop: 20
  },
  titulo: {
    padding: 55,
  },
  lista: {
    height: 150,
  },
  containerLista:{
    flex: 1,
    margin: 15,
    padding: 30,
    flexDirection: 'row',
    //alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: 'rgb(58, 203, 198)',
  },
})
