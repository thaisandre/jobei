import React, { Component } from 'react';

import { Platform, StyleSheet, Text, View, ScrollView, Image,
  TextInput, AsyncStorage, TouchableOpacity, PixelRatio, Alert,
  ActivityIndicator, Modal} from 'react-native';

import { Container, Content, Header, Body, Toast, Root, Picker,
  Form, Icon, Textarea, Left, Right, Title, Button } from 'native-base';

import AppIntroSlider from 'react-native-app-intro-slider';
import ImagePicker from 'react-native-image-picker';
import MultiSelect from 'react-native-multiple-select';
import Loader from '../components/loader';
import Base64 from "../config/Base64";
import {apiURI, updateCandidato, getCargos} from '../config/JobeiApi';
import axios from 'axios';
import { onSignIn } from "../config/auth";
import {showToastBottom, showToastTop} from '../utils/helpers';

const token = "";
const idCandidato = "";

console.disableYellowBox = true;

export default class EditarDados extends Component {

  

  constructor(props) {
    super(props);

    this.state = {
      nome: this.props.navigation.state.params.nome,
      nomeError: false,
      sobrenome: this.props.navigation.state.params.sobrenome,
      sobrenomeError: false,
      email: this.props.navigation.state.params.email,
      emailError: false,
      celular: this.props.navigation.state.params.celular,
      celularError: false,
      senha: this.props.navigation.state.params.senha,
      senhaError: false,
      endereco: this.props.navigation.state.params.endereco,
      enderecoError: false,
      cep: '',
      cepError: false,
      numero: '',
      numeroError: false,
      descricao: this.props.navigation.state.params.descricao,
      descricaoError: false,
      cargo: this.props.navigation.state.params.cargo.nome,
      cargos: [],
      habilidades: this.props.navigation.state.params.habilidades,
      valores: this.props.navigation.state.params.valores,
      foto: this.props.navigation.state.params.foto,
      imagem64: this.props.navigation.state.params.imagem64,
      loading: false,
    }
  }

  onValueChange(value: string) {
    this.setState({
      cargo: value
    })
  }

    parseJwt(token) {
      var base64Url = token.split(".")[1];
      var base64 = base64Url.replace("-", "+").replace("_", "/");
      return JSON.parse(Base64.atob(base64));
    }

    async init() {
      this.token = await AsyncStorage.getItem("token");
      this.idCandidato = this.parseJwt(this.token).sub;
    }

    getEndereco = (cep, numero) => {
      axios.get('https://viacep.com.br/ws/'+cep+'/json/')
      .then(res => {
        enderecoJSON = res.data;
        if (enderecoJSON.logradouro === undefined){
          this.setState({enderecoError: true});
          showToastTop('Não foi possível encotrar o endereço com este CEP. Tente novamente.');
        } else {
          this.setState({enderecoError: false});
          this.setState({endereco: enderecoJSON.logradouro + ", " + numero +
            " - " + enderecoJSON.localidade +  ", " + enderecoJSON.uf});
       }
      })
      .catch(error => {
        showToastTop("Não foi possível localizar o endereço. O servidor está em manutenção. Tente novamente mais tarde");
      });
    }

    componentDidMount() {
      this.pegarCargos();
    }

    pegarCargos(){
      getCargos()
      .then(res => {
        this.setState({cargos: res});
      })
      .catch(error => {
        showToastTop('Não foi possível carregar as habilidades. O servidor está em manutenção. Tente novamente mais tarde.');
      });
    }


    async alterarCandidato(){
      this.setState({loading: true});
      var habs = [];
      this.state.habilidades.forEach(x => habs.push({nome: x.nome}));

      var vals = [];
      this.state.valores.forEach(x => vals.push({nome: x.nome}));

      await this.init();
      var user = {
        nome: this.state.nome,
        sobrenome: this.state.sobrenome,
        email: this.state.email,
        celular: this.state.celular,
        cargo: this.state.cargo,
        endereco: this.state.endereco,
        descricao: this.state.descricao,
        habilidades: habs,
        valores: vals,
        imagem64: this.state.imagem64,
      }

      //console.warn(user.nome);
      //console.warn(user.sobrenome);
      //console.warn(user.email);
      //console.warn(user.celular);
      //console.warn(user.cargo);
      //console.warn(user.endereco);
      //console.warn(user.descricao);
      //console.warn(user.habilidades);
      //console.warn(user.valores);
      //console.warn(user.imagem64);

      await updateCandidato(user, this.token, this.idCandidato)
          .then(res => {
            this.setState({loading: false});
            //showToastTop('Dados atualizados com sucesso');
            this.props.navigation.navigate('Perfil');
          })
          .catch(error => {
            this.setState({loading: false});
            showToastTop('não foi possível editar o(s) dado(s)');
          });
      }

      validate(texto, type) {
        alph = /^[a-zA-ZáàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ ]+$/
        email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        senha = /^(?=.{6,}$)/
        celular = /^[0-9]{2}9[0-9]{8}$/
        email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        cep = /^(?=.{8,8}$)/
        number = /^[0-9\b]+$/

        if (type == 'nome') {
          if (alph.test(texto)) {
            this.setState({
              nomeError: false,
            })
          }
          else {
            this.setState({
              nomeError: true,
            })
            showToastTop('Nome inválido');
          }
        }
        else if (type == 'sobrenome') {
          if (alph.test(texto)) {
            this.setState({
              sobrenomeError: false,
            })

          }
          else {
            this.setState({
              sobrenomeError: true,
            })
            showToastTop('Sobrenome inválido');
          }0
        }
        else if(type == 'celular'){
          if(celular.test(texto)){
            this.setState({
                celularError: false,
            })

          }
          else{
            this.setState({
                celularError: true,
            })
            showToastTop('Telefone Inválido. Entre com apenas números');
          }
        }
        else if (type == 'email') {
          if (email.test(texto)) {
            this.setState({
              emailError: false,
            })
          }
          else {
            this.setState({
              emailError: true,
            })
            showToastTop('Entre com um email válido');
          }
        }
        else if (type == 'senha') {
          if (senha.test(texto)) {
            this.setState({
              senhaError: false,
            })
          }
          else {
            this.setState({
              senhaError: true,
            })
            showToastTop('A senha deve conter mais de 6 caracteres');
          }
        }
        else if(type == 'cep'){
          if(cep.test(texto)){
            this.setState({
              cepError: false,
            })

          }
          else{
            this.setState({
              cepError: true,
            })
            showToastTop('CEP Inválido. Apenas números são permitidos.');
          }
        }
        else if(type == 'numero'){
          if(number.test(texto)){
            this.setState({
              numeroError: false,
            })

          }
          else{
            this.setState({
              numeroError: true,
            })
            showToastTop('Apenas números são permitidos');
          }
        }
        else if(type == 'descricao'){
          if(this.state.descricao.length >= 5 && this.state.descricao.length <= 300){
            this.setState({
            descricaoError: false,
            })
          }
          else {
            this.setState({
              descricaoError: true,
            })
            showToastTop('A descrição deve ter entre 5 e 300 caracteres')
          }
      }
    }


  render() {

    const cargosOrdenados = this.state.cargos.sort(function(a, b){
        return a.nome < b.nome ? -1 : a.nome > b.nome ? 1 : 0;
    });

    return (
    <Root>
      <Loader loading={this.state.loading} />
    <ScrollView>

        <View style={styles.container}>
        <View style={styles.titulo}>
          <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Dados Pessoais</Text>
        </View>

        <View style={styles.lineStyle} />

        <View style={styles.inputContainer}>
          <TextInput placeholder={this.state.nome} style={[styles.inputs,
          this.state.nomeError ? styles.erroBorder : null]}
            onChangeText={(nome) => this.setState({ nome })}
            onEndEditing={() => this.validate(this.state.nome, 'nome')}
            value={this.state.nome} />

          <TextInput placeholder={this.state.sobrenome} style={[styles.inputs,
          this.state.sobrenomeError ? styles.erroBorder : null]}
            onChangeText={(sobrenome) => this.setState({ sobrenome })}
            value={this.state.sobrenome}
            onEndEditing={() => this.validate(this.state.sobrenome, 'sobrenome')}
          />

          {/*}<TextInput placeholder={this.state.email} style={[styles.inputs,
          this.state.emailError ? styles.erroBorder : null]}
            onChangeText={(email) => this.setState({ email })}
            value={this.state.email}
            autoCapitalize='none'
            onEndEditing={() => this.validate(this.state.email, 'email')}
          />*/}

          <TextInput placeholder={this.state.celular} style={[styles.inputs,
          this.state.celularError ? styles.erroBorder : null]}
            onChangeText={(celular) => this.setState({ celular })}
            value={this.state.celular}
            autoCapitalize='none'
            keyboardType={'number-pad'}
            onEndEditing={() => this.validate(this.state.celular, 'celular')}
          />

          </View>


          <View style={styles.titulo2}>
            <Text style={{fontSize: 16, fontWeight: 'bold'}}>Endereço</Text>
          </View>

            <View style={styles.lineStyle} />

          <View style={styles.inputContainer}>
          <TextInput placeholder='endereço' style={styles.inputsEndereco}
            onChangeText={texto => this.setState({endereco: texto})}
            autoCapitalize='none'
            editable={false}
            value={this.state.endereco}
            />

            <TextInput placeholder='cep' style={[styles.inputs,
             this.state.cepError? styles.erroBorder: null]}
              onChangeText={(cep) => this.setState({cep})}
              onEndEditing={() => this.validate(this.state.cep, 'cep')}
              value={this.state.cep}
              keyboardType={'number-pad'}
              autoCapitalize='none' />

            <TextInput placeholder='numero' style={[styles.inputs,
             this.state.numeroError? styles.erroBorder: null]}
              onChangeText={(numero) => this.setState({numero})}
              onEndEditing={() => this.validate(this.state.numero, 'numero')}
              value={this.state.numero}
              keyboardType={'number-pad'}
              />

              <TouchableOpacity onPress = {() => this.getEndereco(this.state.cep, this.state.numero)}>
                <View style={{borderColor: 'rgb(0, 163, 151)', borderRadius: 5, borderWidth: 1,
                marginTop: 20, width: '90%'}}>
                  <Text style={{fontSize: 12, padding: 10, height: 40, fontWeight: 'bold', color: 'rgb(0, 163, 151)',
                  textAlign: 'center', paddingBottom: 5}}>
                    TROCAR ENDEREÇO
                  </Text>
                </View>
              </TouchableOpacity>

          </View>

          <View style={styles.titulo2}>
            <Text style={{fontSize: 16, fontWeight: 'bold'}}>Dados Profissionais</Text>
          </View>

            <View style={styles.lineStyle} />
          <View style={styles.inputContainer}>

          <View style={styles.inputs}>
             <Form>
               <Picker
                renderHeader={backAction =>
                  <Header>
                  <Left>
                    <Button transparent onPress={backAction}>
                      <Icon name="arrow-back" style={{ color: "#007aff" }} />
                    </Button>
                  </Left>
                  <Body style={{alignItems:'center'}}><Title>Cargo</Title></Body>
                  <Right/>
                </Header>}
                mode="dropdown"
                 //iosIcon={<Icon name="ios-arrow-down-outline" />}
                 style={{ width: undefined, textAlign: 'center'}}
                 headerBackButtonText="voltar"
                 placeholder={this.state.cargo.nome}
                 placeholderStyle={{ color: '#000'}}
                 placeholderIconColor="#CCC"
                 textStyle={{ fontSize: 14, textAlign: 'center', marginBottom: '5%' }}
                 selectedValue={this.state.cargo}
                 onValueChange={this.onValueChange.bind(this)}
               >
               {cargosOrdenados.map((cargo) => (
                 <Picker.Item label={cargo.nome} value={cargo.nome}/>
               ))}
               </Picker>
             </Form>
           </View>


           <Form style={{width: '90%'}}>
            <Textarea rowSpan={5} placeholder="Fale um pouco sobre você..."
            placeholderTextColor='#000'
            style={[styles.textArea,
            this.state.descricaoError? styles.erroBorder: null]}
            onChangeText={(descricao) => this.setState({descricao})}
            onEndEditing={() => this.validate(this.state.descricao, 'descricao')}
            value={this.state.descricao}
            autoCapitalize='none'/>
           </Form>

          <View style={styles.lineStyle} />

           <View style={styles.inputContainer2}>
             <TouchableOpacity onPress = {() => this.alterarCandidato()}>
               <View style={{borderColor: 'rgb(0, 163, 151)', borderRadius: 5, borderWidth: 1,
               marginTop: 20, width: '90%'}}>
                 <Text style={{fontSize: 12, padding: 10, height: 40, fontWeight: 'bold', color: 'rgb(0, 163, 151)',
                 textAlign: 'center', paddingBottom: 5}}>
                   EDITAR
                 </Text>
               </View>
             </TouchableOpacity>
           </View>

        </View>

      </View>

      </ScrollView>
      </Root>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    backgroundColor: 'white',
  },
  lineStyle: {
    marginTop: '3%',
    borderBottomColor: 'rgb(58, 203, 198)',
    borderBottomWidth: 1
  },
  inputContainer: {
    alignItems: 'center',
  },
  inputContainer2: {
      alignItems: 'center',
      marginTop: 20
    },
  inputs: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: 'center',
    height: 40,
    //borderColor: '#8a8c91',
    borderColor: '#9B9B9B',
    //borderColor: 'rgb(58, 203, 198)',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    width: '90%',
    borderRadius: 5
  },
  inputsEndereco: {
    paddingBottom: 5,
    marginTop: 20,
    textAlign: 'center',
    height: 40,
    backgroundColor: '#ffffff',
    borderBottomColor: '#9B9B9B',
    width: '90%',
  },
  botao: {
    backgroundColor: 'rgb(58, 203, 198)',
    marginTop: 20
  },
  titulo: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  titulo2: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: 30,
  },
  erroBorder: {
    borderWidth: 1,
    borderColor: 'red',
  },
  erroText: {
    color: 'red',
  },
  avatarContainer: {
    borderColor: '#9B9B9B',
    borderWidth: 1 / PixelRatio.get(),
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
  textArea: {
    marginTop: 20,
    borderColor: '#8a8c91',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    height: 120,
    borderRadius: 5,
    fontSize: 14,

  },
});
