import React, { Component } from 'react';
import { Text, ScrollView, AsyncStorage } from 'react-native';
import { List, ListItem } from 'react-native-elements';

import {onSignOut} from '../config/auth';
import axios from "axios";
import {apiURI, alterarCandidato, getCandidato} from '../config/JobeiApi';
import {showToastBottom, showToastTop} from '../utils/helpers';
import Base64 from "../config/Base64";
import { Buffer } from 'buffer'
import {withNavigation, withNavigationFocus, NavigationEvents} from 'react-navigation';
const token = "";
const idCandidato = "";

class Settings extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: [],
      nome: '',
      sobrenome: '',
      endereco: '',
      email: '',
      descricao: '',
      celular: '',
      cargo: [],
      habilidades: [],
      valores: [],
      imagem: '',
      imagem64: '',
    }
  }

  async logout(){
    await AsyncStorage.removeItem('token');
    this.props.navigation.navigate('UserLogedOut');
  }

  async componentDidMount() {
    await this.init();
    let perfilApi = null;

    axios.get( apiURI + '/candidatos/' + this.idCandidato,
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + this.token
    }})
    .then(res => {
      perfilApi = res.data;
      this.setState({user: perfilApi});
      this.setState({nome: perfilApi.nome});
      this.setState({sobrenome: perfilApi.sobrenome});
      this.setState({endereco: perfilApi.endereco});
      this.setState({email: perfilApi.email});
      this.setState({celular: perfilApi.celular});
      this.setState({descricao: perfilApi.descricao});
      this.setState({cargo: perfilApi.cargo});
      this.setState({habilidades: perfilApi.habilidades});
      this.setState({valores: perfilApi.valores});
      this.setState({imagem: perfilApi.imagem});

      axios.get(this.state.imagem,
        {
          responseType: 'arraybuffer'
        }
      )
        .then(res => {
          var oi = new Buffer(res.data, 'binary').toString('base64')
          this.setState({imagem64: 'data:image/jpeg;base64,' + oi});
        })
        .catch(error => {
          showToastTop('Não foi possível carregar o Perfil. Tente novamente mais tarde');
        });
    })
    .catch(error => {
      showToastTop('Não foi possível carregar o Perfil. Tente novamente mais tarde');
    });
  }

  getBase64(url) {
    return axios
    .get(url, {
      responseType: 'arraybuffer'
    })
    .then(response => new Buffer(response.data, 'binary').toString('base64'))
  }

  async pegarCandidato() {
    await this.init();
    getCandidato(this.token, this.idCandidato)
      .then(res => {
        //console.warn(res);
        this.setState({nome: res.nome});
        this.setState({sobrenome: res.sobrenome});
        this.setState({endereco: res.endereco});
        this.setState({email: res.email});
        this.setState({celular: res.celular});
        this.setState({descricao: res.descricao});
        this.setState({cargo: res.cargo});
        this.setState({habilidades: res.habilidades});
        this.setState({valores: res.valores});
        this.setState({imagem:res.imagem});
      })
      .catch(error => {
        showToastTop('Não foi possível carregar os dados. O servidor está em manutenção. Tente novamente mais tarde.');
      });
  }

  parseJwt(token) {
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace("-", "+").replace("_", "/");
    return JSON.parse(Base64.atob(base64));
  }

  async init() {
    this.token = await AsyncStorage.getItem("token");
    this.idCandidato = this.parseJwt(this.token).sub;
  }

  goToScreen = (screenName) => {
    this.props.navigation.navigate(screenName, {
      nome: this.state.nome,
      sobrenome: this.state.sobrenome,
      email: this.state.email,
      senha: this.state.senha,
      cargo: this.state.cargo,
      descricao: this.state.descricao,
      imagem: this.state.imagem,
      imagem64: this.state.imagem64,
      avatarSource: this.state.avatarSource,
      celular: this.state.celular,
      endereco: this.state.endereco,
      habilidades: this.state.habilidades,
      valores: this.state.valores,
      user: this.state.user,
      }
    );
  }

  render() {

    return (
      <ScrollView>
      <NavigationEvents
          onWillFocus={() => this.pegarCandidato()}/>

        <List>
          <ListItem
            title="Editar Foto"
            onPress={() => this.goToScreen('EditarFoto')}
            rigthIcon={{name: 'arrow-downward'}}
          />
          <ListItem
            title="Editar Dados"
            onPress={() => this.goToScreen('EditarDados')}
            rigthIcon={{name: 'arrow-downward'}}
          />

          <ListItem
            title="Editar Habilidades"
            onPress={() => this.goToScreen('EditarHabilidades')}
          />
          <ListItem
            title="Editar Valores"
            onPress={() => this.goToScreen('EditarValores')}
          />
        </List>

        <List>
          <ListItem
            title='Alterar senha'
            onPress={() => this.goToScreen('Senha')}
            />

          <ListItem
            title="Logout"
            onPress={() => this.logout()}
            rightIcon={{ name: 'cancel' }}
          />
        </List>
      </ScrollView>
    );
  }
}

export default withNavigationFocus(Settings);
