import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, TouchableOpacity, PixelRatio, Alert } from 'react-native';
import { Root } from 'native-base';
import { cargos, imagem_padrao } from '../config/data';
import ImagePicker from 'react-native-image-picker';
import {showToastBottom, showToastTop} from '../utils/helpers';

export default class DadosPessoais extends Component{
    constructor(props) {
        super(props);

        this.state = {
          celular: '',
          celularError: false,
          email: '',
          emailError: false,
          senha: '',
          senhaError: false,
        }
    }

    goToScreen = (user) => {

        if(this.state.celularError === false && this.state.celular != null && this.state.celular != ''
         && this.state.emailError === false && this.state.email != null && this.state.email != ''
         && this.state.senhaError === false && this.state.senha != null && this.state.senha != '') {

        this.props.navigation.navigate('Endereco',
          {
            nome: user.nome,
            sobrenome: user.sobrenome,
            avatarSource: user.avatarSource,
            foto: user.foto,
            email: this.state.email,
            senha: this.state.senha,
            celular: this.state.celular
          });
        } else {
         showToastTop('Preencha os dados corretamente antes de continuar');
        }
    };

    validate(texto, type){
        // telefone = /^(?:\d{2}9\d{8}|\((?:1[2-9]|[2-9]\d)\) [5-9]\d{3}-\d{4})$/
        celular = /^[0-9]{2}9[0-9]{8}$/
        email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        senha = /^(?=.{6,}$)/
        if(type == 'celular'){
          if(celular.test(texto)){
            this.setState({
                celularError: false,
            })

          }
          else{
            this.setState({
                celularError: true,
            })
            showToastTop('Telefone Inválido. Entre com apenas números');
          }
        }
        else if (type == 'email') {
          if (email.test(texto)) {
            this.setState({
              emailError: false,
            })
          }
          else {
            this.setState({
              emailError: true,
            })
            showToastTop('Entre com um email válido');
          }
        }
        else if (type == 'senha') {
          if (senha.test(texto)) {
            this.setState({
              senhaError: false,
            })
          }
          else {
            this.setState({
              senhaError: true,
            })
            showToastTop('A senha deve conter mais de 6 caracteres');
          }
        }
    }

    render() {

        return(
            <Root>
                <View style={styles.container}>
                    <View style={styles.titulo}>
                        <Text style={{ fontSize: 16, fontWeight: 'bold' }}>Dados Pessoais</Text>

                        <View style={{ flexDirection: 'row', marginLeft: '34%' }}>
                            <TouchableOpacity onPress={() => this.goToScreen(this.props.navigation.state.params)}>
                            <Text style={{ fontSize: 12, padding: 5, fontWeight: 'bold', color: 'rgb(58, 203, 198)' }}>CONTINUE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.lineStyle} />

                    <View style={styles.inputContainer}>

                        <TextInput placeholder='11999999999' style={[styles.inputs,
                        this.state.celularError ? styles.erroBorder : null]}
                        onChangeText={(celular) => this.setState({ celular })}
                        value={this.state.celular}
                        autoCapitalize='none'
                        keyboardType={'phone-pad'}
                        onEndEditing={() => this.validate(this.state.celular, 'celular')}
                        />

                        <TextInput placeholder='E-mail' style={[styles.inputs,
                        this.state.emailError ? styles.erroBorder : null]}
                        onChangeText={(email) => this.setState({ email })}
                        value={this.state.email}
                        autoCapitalize='none'
                        onEndEditing={() => this.validate(this.state.email, 'email')}
                        />

                        <TextInput placeholder='Senha' style={[styles.inputs,
                        this.state.senhaError ? styles.erroBorder : null]}
                        onChangeText={(senha) => this.setState({ senha })}
                        value={this.state.senha}
                        autoCapitalize='none'
                        secureTextEntry={true}
                        onEndEditing={() => this.validate(this.state.senha, 'senha')}
                        />

                    </View>
                </View>
            </Root>
        );
    }
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      padding: 40,
      backgroundColor: 'white',
    },
    lineStyle: {
      marginTop: '3%',
      borderBottomColor: 'rgb(58, 203, 198)',
      borderBottomWidth: 1
    },
    inputContainer: {
      alignItems: 'center'
    },
    inputs: {
      paddingBottom: 5,
      marginTop: 20,
      textAlign: 'center',
      height: 40,
      //borderColor: '#8a8c91',
      borderColor: '#9B9B9B',
      //borderColor: 'rgb(58, 203, 198)',
      backgroundColor: '#ffffff',
      borderWidth: 1,
      width: '90%',
      borderRadius: 5
    },
    botao: {
      backgroundColor: 'rgb(58, 203, 198)',
      marginTop: 20
    },
    titulo: {
      flexDirection: 'row',
      alignItems: 'flex-start',
    },
    erroBorder: {
      borderWidth: 1,
      borderColor: 'red',
    },
    erroText: {
      color: 'red',
    },
  });
