import React, { Component } from 'react';
import axios from 'axios';
import { apiURI } from '../config/JobeiApi';
import { showToastTop } from '../utils/helpers';
import { Root, Content } from 'native-base';
import { Form, TextValidator } from 'react-native-validator-form';
import { Platform, StyleSheet, Text, View, ScrollView, Image, TextInput,
  AsyncStorage, Button, TouchableOpacity, PixelRatio, Alert } from 'react-native';

import Base64 from "../config/Base64";

const token = "";
const idCandidato = "";

export default class Senha extends Component {

    constructor(props) {
        super(props);

        this.state = {
            senhaAtual: '',
            senhaAtualError: false,
            senha: '',
            senhaError: false,
            confirmarSenha: '',
            confirmarSenhaError: false,
            user: {},
        };

        this.handlePassword = this.handlePassword.bind(this);
        this.handleRepeatPassword = this.handleRepeatPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async logout(){
      await AsyncStorage.removeItem('token');
      this.props.navigation.navigate('UserLogedOut');
    }

    componentWillMount() {
      Form.addValidationRule('isPasswordMatch', (value) => {
        if(value !== this.state.senha) {
          return false;
        }
        return true;
      });
    }

    async putSenha() {
      await this.init();
      //console.warn(this.state.senhaAtual);
      //console.warn(this.state.senha);
      //console.warn(this.state.confirmarSenha);

      if(this.state.senhaAtualError === false && this.state.senhaAtual != ''
        && this.state.senhaError === false && this.state.senha != ''
        && this.state.confirmarSenhaError === false && this.state.confirmarSenha != '') {

          axios.put(apiURI + '/candidatos/' + this.idCandidato + '/alterarSenha',
            {
                senhaAtual: this.state.senhaAtual,
                senhaNova: this.state.senha,
            },
            {
                headers: {
                    'Content-Type': 'application/json',
                    'api-version': '1.0',
                    'Authorization': 'Bearer ' + this.token
                }
            })
            .then(res => {
              console.warn(res.data);
                showToastTop('Senha alterada com sucesso!');
                this.logout();

            })
            .catch(error => {
                showToastTop(error.response.data);
            })

      } else {
        showToastTop('Preencha os dados corretamente');
      }
    }



    handlePassword(event) {
        const {user} = this.state;
        user.senha = event.nativeEvent.text;
        //isso ta certo?
        this.setState({ senha });
    }

    handleRepeatPassword(event) {
        const {user} = this.state;
        user.confimarSenha = event.nativeEvent.text;
        //isso ta certo?????? AAAAAA
        this.setState({ confimarSenha });
    }


    handleSubmit() {
        this.refs.form.submit();
    }

    validate(texto, type) {
      senha = /^(?=.{6,}$)/

      if (type == 'senhaAtual') {
        if (senha.test(texto)) {
          this.setState({
            senhaAtualError: false,
          })
        }
        else {
          this.setState({
            senhaAtualError: true,
          })
          showToastTop('Senha atual inválida');
        }
      }
      else if (type == 'senha') {
        if (senha.test(texto) && texto != this.state.senhaAtual) {
          this.setState({
            senhaError: false,
          })
        }
        else {
          this.setState({
            senhaError: true,
          })
          showToastTop('Senha inválida');
        }
      }
      else if (type == 'confirmarSenha') {
        if (senha.test(texto) && texto == this.state.senha) {
          this.setState({
            confirmarSenhaError: false,
          })
        }
        else {
          this.setState({
            confirmarSenhaError: true,
          })
          showToastTop('Senha inválida');
        }
      }
    }

    parseJwt(token) {
      var base64Url = token.split(".")[1];
      var base64 = base64Url.replace("-", "+").replace("_", "/");
      return JSON.parse(Base64.atob(base64));
    }

    async init() {
      this.token = await AsyncStorage.getItem("token");
      this.idCandidato = this.parseJwt(this.token).sub;
    }

    render() {

      return (
        <Root>
        <Content style={{backgroundColor: 'white'}}>

        <View style={styles.inputContainer}>
          <Text style={styles.item}>SENHA ATUAL</Text>
          <TextInput placeholder='Senha Atual' style={[styles.inputs,
            this.state.senhaAtualError ? styles.erroBorder : null]}
            onChangeText={(senhaAtual) => this.setState({ senhaAtual })}
            value={this.state.senhaAtual}
            autoCapitalize='none'
            secureTextEntry={true}
            onEndEditing={() => this.validate(this.state.senhaAtual, 'senhaAtual')} />

          <Text style={styles.item}>SENHA NOVA</Text>
          <TextInput placeholder='Senha Nova' style={[styles.inputs,
            this.state.senhaError ? styles.erroBorder : null]}
            onChangeText={(senha) => this.setState({ senha })}
            value={this.state.senha}
            autoCapitalize='none'
            secureTextEntry={true}
            onEndEditing={() => this.validate(this.state.senha, 'senha')}/>

          <TextInput placeholder='Confimar Senha' style={[styles.inputs,
            this.state.confirmarSenhaError ? styles.erroBorder : null]}
            onChangeText={(confirmarSenha) => this.setState({ confirmarSenha })}
            value={this.state.confirmarSenha}
            autoCapitalize='none'
            secureTextEntry={true}
            onEndEditing={() => this.validate(this.state.confirmarSenha, 'confirmarSenha')}/>

      </View>
      <View style={styles.inputContainer2}>
             <TouchableOpacity onPress = {() => this.putSenha()}>
               <View style={{borderColor: 'rgb(0, 163, 151)', borderRadius: 5, borderWidth: 1,
               marginTop: 20, width: '90%'}}>
                 <Text style={{fontSize: 12, padding: 10, height: 40, fontWeight: 'bold', color: 'rgb(0, 163, 151)',
                 textAlign: 'center', paddingBottom: 5}}>
                   ALTERAR
                 </Text>
               </View>
             </TouchableOpacity>
           </View>
      </Content>
      </Root>
    )
  }
}

const styles = StyleSheet.create({
container: {
  flex: 1,
  padding: 40,
  backgroundColor: 'white',
},
lineStyle: {
  marginTop: '3%',
  borderBottomColor: 'rgb(58, 203, 198)',
  borderBottomWidth: 1
},
inputContainer: {
  alignItems: 'flex-start',
  marginLeft: 35
},

inputContainer2: {
    alignItems: 'center',
    marginTop: 40
  },
inputs: {
  paddingBottom: 5,
  marginTop: 10,
  textAlign: 'center',
  height: 40,
  //borderColor: '#8a8c91',
  borderColor: '#9B9B9B',
  //borderColor: 'rgb(58, 203, 198)',
  backgroundColor: '#ffffff',
  borderWidth: 1,
  width: '90%',
  borderRadius: 5
},
botao: {
  backgroundColor: 'rgb(58, 203, 198)',
  marginTop: 20,

},
titulo: {
  flexDirection: 'row',
  alignItems: 'center',
},
erroBorder: {
  borderWidth: 1,
  borderColor: 'red',
},
erroText: {
  color: 'red',
},
avatarContainer: {
  borderColor: '#9B9B9B',
  borderWidth: 1 / PixelRatio.get(),
  justifyContent: 'center',
  alignItems: 'center',
},
avatar: {
  borderRadius: 50,
  width: 100,
  height: 100,
},
item: {
    color: "rgb(0, 163, 151)",
     fontSize:14,
     marginTop:40,

},
})
