import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, Image, TextInput, AsyncStorage, TouchableOpacity} from 'react-native';
import {Container, Content, Header, Body, Toast, Root, Picker, Form, Icon} from 'native-base';

import axios from 'axios';
//import {getEnderecoApi} from '../config/restApi';
//import { valores } from '../config/data';
import { onSignIn } from "../config/auth";
import MultiSelect from 'react-native-multiple-select';
import {apiURI} from '../config/JobeiApi';
import {showToastTop, showToastBottom} from '../utils/helpers';

export default class Valores extends Component {

  constructor(props){
    super(props);

    this.state = {
      valores: [],
      selectedItems: [],
    }
  }

  onSelectedItemsChange = selectedItems => {
   this.setState({ selectedItems });
  };

  async componentDidMount() {
      axios.get(apiURI + '/valores',
        {headers: {
          'Content-Type': 'application/json',
          'api-version': '1.0',
      }})
      .then(res => {
        let valoresApi = res.data
        this.setState({valores: valoresApi});
      })
      .catch(error => {
        showToastTop("Não foi possível carregar as habilidades. O servidor está em manutenção. Tente novamente mais tarde.");
      });
    }

  goToScreen = (user) => {
    if(this.state.selectedItems.length < 5){
      showToastTop('escolha 5 valores');
    } else {
    this.props.navigation.navigate('FinalCadastro',
      {nome: user.nome, sobrenome: user.sobrenome, email: user.email, senha: user.senha,
        cargo: user.cargo, descricao: user.descricao, celular: user.celular, endereco: user.endereco,
        foto: user.foto, avatarSource: user.avatarSource, habilidades: user.habilidades,
        valores: this.state.selectedItems});
      }
  };

  render() {

    const { foto, nome, sobrenome, email, senha, avatarSource, cargo, descricao, endereco, habilidades, celular} = this.props.navigation.state.params;
    const { selectedItems } = this.state;

    if(this.state.selectedItems.length > 5){
      this.state.selectedItems.pop();
    }

    return(
      <Root>
      <Content style={{backgroundColor: 'white'}}>
       <ScrollView style={styles.container}>

         <View style={{flexDirection: 'row'}}>
           <Text style={{fontSize: 16, fontWeight: 'bold',
               alignItems: 'flex-start'}}>Valores</Text>
           <View style={{flexDirection: 'row', marginLeft: '55%'}}>
             <TouchableOpacity onPress = {() => this.goToScreen(this.props.navigation.state.params)}>
             <Text style={{fontSize: 12, padding: 5, fontWeight: 'bold', color: 'rgb(58, 203, 198)'}}>CONTINUE</Text>
             </TouchableOpacity>
           </View>
         </View>
         <View style={styles.lineStyle} />
          <View style={{ flex: 1}}>
          <View>
          {this.multiSelect? this.multiSelect.getSelectedItemsExt(selectedItems): null}
          </View>
          <View style={{marginTop: 30}}>
             <MultiSelect
               hideTags
               items={this.state.valores}
               uniqueKey="nome"
               ref={(component) => { this.multiSelect = component }}
               onSelectedItemsChange={this.onSelectedItemsChange}
               selectedItems={selectedItems}
               selectText="selecione 5 valores"
               searchInputPlaceholderText="Search Items..."
               onChangeInput={ (text)=> console.log(text)}
               tagRemoveIconColor="#CCC"
               tagBorderColor="rgb(58, 203, 198)"
               tagTextColor="rgb(58, 203, 198)"
               selectedItemTextColor="rgb(58, 203, 198)"
               selectedItemIconColor="rgb(58, 203, 198)"
               itemTextColor="#000"
               displayKey="nome"
               searchInputStyle={{ color: '#CCC' }}
               submitButtonColor="rgb(58, 203, 198)"
               submitButtonText="confirmar"
             />
             </View>

           </View>

       </ScrollView>
     </Content>
     </Root>
    );
  }
 }

 const styles = StyleSheet.create({
   container: {
     flex: 1,
     //justifyContent: 'center',
     //alignItems: 'center',
     padding: 40,
   },
   lineStyle: {
     marginTop: '3%',
     marginBottom: '3%',
     borderBottomColor: "rgb(58, 203, 198)",
     borderBottomWidth: 1
   },
   inputContainer: {
     alignItems: 'center'
   },
   inputs: {
     paddingBottom: 5,
     marginTop: 20,
     height: 40,
     borderColor: '#8a8c91',
     backgroundColor: '#ffffff',
     borderWidth: 1,
     width: '90%',
     borderRadius: 5
   },
   botao: {
     backgroundColor: "rgb(58, 203, 198)",
     marginTop: 20
   },
   titulo: {
     padding: 55,
   },
   lista: {
     height: 150,
   },
   containerLista:{
     flex: 1,
     margin: 15,
     padding: 30,
     flexDirection: 'row',
     //alignItems: 'center',
     borderRadius: 5,
     borderWidth: 1,
     borderColor: "rgb(58, 203, 198)",
   },
 })
