'use strict';
import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import { Toast, Root} from 'native-base';

export function showToastBottom(message) {
  return (
    Toast.show({
      text: message,
      buttonText: 'OK',
      duration: 5000,
      position: 'bottom',
      textStyle: {color: 'white'},
      type: 'warning'
    })
  );
}

export function showToastTop(message) {
  return (
    Toast.show({
      text: message,
      buttonText: 'OK',
      duration: 5000,
      position: 'top',
      textStyle: {color: 'white'},
      type: 'warning'
    })
  );
}
