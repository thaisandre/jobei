export const me = {
    id: '3',
    nome: 'Thais',
    sobrenome: 'André',
    celular: '11998143557',
    descricao: 'minha descrição',
    email: 'thais@gmail.com',
    cargo: 'Desenvolvedor Back-End',
    endereco: 'Alameda Barão de Limeira, 539 - São Paulo, SP',
    habilidades: [
        {nome: 'back-end'},
        {nome: 'dev-ops'},
        {nome: 'organização'},
        {nome: 'clen-code'},
        {nome: 'boas práticas'}
    ],
    valores: [
      {nome: 'ética'},
      {nome: 'transparência'},
      {nome: 'segurança'},
      {nome: 'respeito'},
      {nome: 'diversidade'}
    ],
    picture: './../resources/img/thais.png',
  }

  export const me2 = {
    id: '1',
    nome: 'Samantha',
    sobrenome: 'Gomes',
    email: 'samantha@gmail.com',
    cargo: 'Desenvolvedor Mobile',
    endereco: 'Alameda Barão de Limeira, 539 - São Paulo, SP',
    habilidades: [
        {nome: 'mobile'},
        {nome: 'front-end'},
        {nome: 'organização'},
        {nome: 'design gráfico'},
        {nome: 'usabilidade'}
    ],
    valores: [
      {nome: 'ética'},
      {nome: 'colaboração'},
      {nome: 'justiça'},
      {nome: 'liderança'},
      {nome: 'diversidade'}
    ],
    picture: './../resources/img/sam.jpeg',
  }

export const users = [
  {
    id: '1',
    nome: 'Samantha',
    sobrenome: 'Gomes',
    email: 'samantha@gmail.com',
    cargo: 'Desenvolvedor Mobile',
    endereco: 'Alameda Barão de Limeira, 539 - São Paulo, SP',
    habilidades: [
        {nome: 'mobile'},
        {nome: 'front-end'},
        {nome: 'organização'},
        {nome: 'design gráfico'},
        {nome: 'usabilidade'}
    ],
    valores: [
      {nome: 'ética'},
      {nome: 'colaboração'},
      {nome: 'justiça'},
      {nome: 'liderança'},
      {nome: 'diversidade'}
    ],
    picture: './../resources/img/sam.jpeg',
  },
  {
    id: '2',
    nome: 'Laisa',
    sobrenome: 'Nicácio',
    email: 'laisa@gmail.com',
    cargo: 'Desenvolvedor Mobile',
    endereco: 'Alameda Barão de Limeira, 539 - São Paulo, SP',
    habilidades: [
        {nome: 'mobile'},
        {nome: 'back-end'},
        {nome: 'organização'},
        {nome: 'clen-code'},
        {nome: 'boas práticas'}
    ],
    valores: [
      {nome: 'ética'},
      {nome: 'competitividade'},
      {nome: 'foco'},
      {nome: 'liderança'},
      {nome: 'diversidade'}
    ],
    picture: './../resources/img/laisa.jpg',
  },
  {
    id: '3',
    nome: 'Thais',
    sobrenome: 'André',
    email: 'thais@gmail.com',
    cargo: 'Desenvolvedor Back-End',
    endereco: 'Alameda Barão de Limeira, 539 - São Paulo, SP',
    habilidades: [
        {nome: 'back-end'},
        {nome: 'dev-ops'},
        {nome: 'organização'},
        {nome: 'clen-code'},
        {nome: 'boas práticas'}
    ],
    valores: [
      {nome: 'ética'},
      {nome: 'transparência'},
      {nome: 'segurança'},
      {nome: 'respeito'},
      {nome: 'diversidade'}
    ],
    picture: './../resources/img/thais.png',
  },
  {
    id: '4',
    nome: 'Vinicius',
    sobrenome: 'Torres',
    email: 'vini@gmail.com',
    cargo: 'Desenvolvedor Mobile',
    endereco: 'Alameda Barão de Limeira, 539 - São Paulo, SP',
    habilidades: [
        {nome: 'mobile'},
        {nome: 'front-end'},
        {nome: 'design gráfico'},
        {nome: 'UX'},
        {nome: 'usabilidade'}
    ],
    valores: [
      {nome: 'ética'},
      {nome: 'transparência'},
      {nome: 'respeito'},
      {nome: 'colaboração'},
      {nome: 'criatividade'}
    ],
      picture: './../resources/img/vini.png',
  },
]

export const vagas = [
 {
   id: '1',
   cargo: 'Desenvolvedor Back-End',
   descricao: 'Construir apis e micro-serviços',
   nomeEmpresa: 'Google',
   endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
   remuneracao: 2000.00,
   porcentagem: '80%',
   habilidades: [
     {nome: 'back-end'},
     {nome: 'dev-ops'},
     {nome: 'organização'},
     {nome: 'clen-code'},
     {nome: 'boas práticas]'}
   ],
   curtiu: true,
   foto: './../resources/img/google.jpg',
 },
 {
   id: '2',
   cargo: 'Desenvolvedor Front-End',
   descricao: 'Construir web sites responsivos',
   nomeEmpresa: 'Facebook',
   endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
   remuneracao: 2000.00,
   porcentagem: '70%',
   habilidades: [
     {nome: 'frony-end'},
     {nome: 'design-gráfico'},
     {nome: 'organização'},
     {nome: 'clen-code'},
     {nome: 'boas práticas]'}
   ],
     curtiu: false,
     foto: './../resources/img/facebook.png',
 },
 {
   id: '3',
   cargosOrdenados: 'Desenvolvedor Mobile',
   descricao: 'Construir apps legais',
   nomeEmpresa: 'Apple',
   endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
   remuneracao: 2000.00,
   porcentagem: '50%',
   habilidades: [
     {nome: 'mobile'},
     {nome: 'ios'},
     {nome: 'organização'},
     {nome: 'clen-code'},
     {nome: 'boas práticas'}
   ],
   curtiu: true,
   foto: './../resources/img/apple.png',

 },
 {
   id: '4',
   cargo: 'Analista de Banco de Dados',
   descricao: 'Manter a integridade dos dados',
   nomeEmpresa: 'Twitter',
   endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
   remuneracao: 2000.00,
   porcentagem: '50%',
   habilidades: [
     {nome: 'banco de dados'},
     {nome: 'dev-ops'},
     {nome: 'organização'},
     {nome: 'big data'},
     {nome: 'data science'}
   ],
   curtiu: false,
   foto: './../resources/img/twiter.png',
 }
];

export const google = {
    id: '1',
    nome: 'Google',
    descricao: 'Empresa de tecnologia',
    email: 'google@google.com',
    endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
    valores: [
      {nome: 'qualidade'},
      {nome: 'liberdade'},
      {nome: 'organização'},
      {nome: 'competência'},
      {nome: 'integridade'}
    ],
    imagem: './../resources/img/google.jpg',
  };

export const empresas = [
  {
    id: '1',
    nome: 'Google',
    descricao: 'Empresa de tecnologia',
    email: 'google@google.com',
    endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
    valores: [
      {nome: 'qualidade'},
      {nome: 'liberdade'},
      {nome: 'organização'},
      {nome: 'competência'},
      {nome: 'integridade'}
    ],
    imagem: './../resources/img/google.jpg',
  },
  {
    id: '2',
    nome: 'Facebook',
    descricao: 'Empresa de tecnologia',
    email: 'fb@facebook.com',
    endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
    valores: [
      {nome: 'conectividade'},
      {nome: 'competitividade'},
      {nome: 'organização'},
      {nome: 'diversidade'},
      {nome: 'adaptação'}
    ],
    imagem: './../resources/img/facebook.png',
  },
  {
    id: '3',
    nome: 'Twitter',
    descricao: 'Empresa de tecnologia',
    email: 'twitter@twitter.com',
    endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
    valores: [
      {nome: 'ética'},
      {nome: 'liberdade'},
      {nome: 'diversidade'},
      {nome: 'comunicação'},
      {nome: 'sustentabilidade'}
    ],
    imagem: './../resources/img/twitter.png',
  },
  {
    id: '4',
    nome: 'Apple',
    descricao: 'Empresa de tecnologia',
    email: 'apple@apple.com',
    endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
    valores: [
      {nome: 'segurança'},
      {nome: 'transparência'},
      {nome: 'qualidade'},
      {nome: 'comunicação'},
      {nome: 'sustentabilidade'}
    ],
    imagem: './../resources/img/apple.png',
  },
]

export const messages = [
  {
    id: '1',
    name: 'Apple',
    avatar: './../resources/img/thais.png',
    last_message: 'lorem ipsum',
    time: '18:30 PM'},
  {
    id: '2',
    name: 'Facebbok Inc.',
    avatar: './../resources/img/sam.jpeg',
    last_message: 'lorem ipsum',
    time: '19:45 PM'},
  {
    id: '3',
    name: 'Totvs',
    avatar: './../resources/img/vini.png',
    last_message: 'lorem ipsum',
    time: '20:14 PM'}
]

export const habilidades = [
  {id: 1, nome: 'back-end'},
  {id: 2, nome: 'dev-ops'},
  {id: 3, nome: 'organização'},
  {id: 4, nome: 'clean-code'},
  {id: 5, nome: 'boas práticas'},
  {id: 6, nome: 'testes'},
  {id: 7, nome: 'sistemas operacionas'},
  {id: 8, nome: 'redes'},
  {id: 9, nome: 'servidores'},
  {id: 10, nome: 'treinamento'},
  {id: 11, nome: 'UX'},
  {id: 12, nome: 'desenvolvimento ágil'},
  {id: 13, nome: 'design thinking'},
  {id: 14, nome: 'usabilidade'},
]

export const valores = [
  {id: 1, nome: 'ética'},
  {id: 2, nome: 'criatividade'},
  {id: 3, nome: 'colaboração'},
  {id: 4, nome: 'diversidade'},
  {id: 5, nome: 'sustentabilidade'},
  {id: 6, nome: 'tranparência'},
  {id: 7, nome: 'solidariedade'},
  {id: 8, nome: 'competitividade'},
  {id: 9, nome: 'conectividade'},
  {id: 10, nome: 'respeito'},
  {id: 11, nome: 'integridade'},
  {id: 12, nome: 'flexibilidade'},
  {id: 13, nome: 'qualidade'},
  {id: 14, nome: 'segurança'},
]

export const cargos = [
  {id: 1, nome: 'Analista de Banco de dados'},
  {id: 2, nome: 'Analista de Redes'},
  {id: 3, nome: 'Analista de Sistemas'},
  {id: 4, nome: 'Cientista da Computação'},
  {id: 5, nome: 'Cientista de Dados'},
  {id: 6, nome: 'Desenvolvedor Back-End'},
  {id: 7, nome: 'Desenvolvedor Front-End'},
  {id: 8, nome: 'Desenvolvedor Full-Stack'},
  {id: 9, nome: 'Desenvolvedor Mobile'},
  {id: 10, nome: 'Desenvolvedor UX'},
  {id: 11, nome: 'Técnico de Informática'},
  {id: 12, nome: 'Técnico de Redes'},
  {id: 13, nome: 'Instrutor de Informática'},
]

export const curtidas = [
  {
    'dCandidato': '1',
    'idVaga': '2',
     'vagaCurtiu': true,
     'candidatoCuritu': true
  },
  {
    'dCandidato': '1',
    'idVaga': '3',
     'vagaCurtiu': false,
     'candidatoCuritu': true
  },
  {
    'dCandidato': '2',
    'idVaga': '1',
     'vagaCurtiu': false,
     'candidatoCuritu': true
  },
  {
    'dCandidato': '2',
    'idVaga': '3',
     'vagaCurtiu': true,
     'candidatoCuritu': true
  },
  {
    'dCandidato': '3',
    'idVaga': '1',
     'vagaCurtiu': false,
     'candidatoCuritu': true
  },
  {
    'dCandidato': '3',
    'idVaga': '4',
     'vagaCurtiu': false,
     'candidatoCuritu': true
  },
  {
    'dCandidato': '4',
    'idVaga': '1',
     'vagaCurtiu': false,
     'candidatoCuritu': true
  },
  {
    'dCandidato': '4',
    'idVaga': '3',
     'vagaCurtiu': true,
     'candidatoCuritu': true
  }
]

export const conversas = {
    id: '1',
    user: {
      id: '1',
      nome: 'Samantha',
      sobrenome: 'Gomes',
      email: 'samantha@gmail.com',
      cargo: 'Desenvolvedor Mobile',
      endereco: 'Alameda Barão de Limeira, 539 - São Paulo, SP',
      habilidades: [
          {nome: 'mobile'},
          {nome: 'front-end'},
          {nome: 'organização'},
          {nome: 'design gráfico'},
          {nome: 'usabilidade'}
      ],
      valores: [
        {nome: 'ética'},
        {nome: 'colaboração'},
        {nome: 'justiça'},
        {nome: 'liderança'},
        {nome: 'diversidade'}
      ],
      picture: './../resources/img/sam.jpeg',
    },
    empresa: {
      id: '4',
      nome: 'Apple',
      descricao: 'Empresa de tecnologia',
      email: 'apple@apple.com',
      endereco: 'R. Artur Azevedo - Pinheiros, São Paulo - SP',
      valores: [
        {nome: 'segurança'},
        {nome: 'transparência'},
        {nome: 'qualidade'},
        {nome: 'comunicação'},
        {nome: 'sustentabilidade'}
      ],
      imagem: './../resources/img/apple.png',
    },
    messages: [
      {
        id: '1',
        valor: 'lorem ipsum',
        time: '18:30 PM',
        enviada: false,
      },
      {
        id: '2',
        valor: 'lorem ipsum',
        time: '18:32 PM',
        enviada: false,
      },
      {
        id: '3',
        valor: 'lorem ipsum',
        time: '18:40 PM',
        envidade: true,
      },
      {
        id: '4',
        valor: 'lorem ipsum',
        time: '18:50 PM',
        enviada: false,
      },
      {
        id: '5',
        valor: 'lorem ipsum',
        time: '19:00 PM',
        enviada: false,
      },
      {
        id: '6',
        valor: 'lorem ipsum',
        time: '19:10 PM',
        enviada: true,
      },
      {
        id: '7',
        valor: 'lorem ipsum',
        time: '19:05 PM',
        enviada: true,
      },
    ]
  }

  export const imagem_padrao = {
    foto: 'data:image/jpeg;base64,/9j/7gAmQWRvYmUAZMAAAAABAwAVBAMGCg0AABQoAAAilwAAM0gAAD04/9sAQwADAQEDAQEDAwEDAwMDAwQGBAQEBAQIBgYFBgkICQkJCAkICgsODAoKDQoICQwRDQ0PDxAQEAoNEhMSEBMPEBAQ/9sAQwEDAwMEBAQIBAQIDwsJCw8QEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQ/8IAEQgCWAJYAwERAAIRAQMRAf/EABwAAQACAwEBAQAAAAAAAAAAAAAFBgMEBwIBCP/EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhADEAAAAf0kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeDUMIMhtGYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEeQBDmkfAADZJYnSZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI4qJEgAAAAA3S1k6AAAAAAAAAAAAAAAAAAAAAAAAAAAAfCqFXPgAAAAAABNF1MwAAAAAAAAAAAAAAAAAAAAAAAAAAPBRSHAAAAAAAABtl+NwAAAAAAAAAAAAAAAAAAAAAAAAAHgoRFAAAAAAAAAA2ToJtgAAAAAAAAAAAAAAAAAAAAAAAAFIIEAAAAAAAAAAG6dEMgAAAAAAAAAAAAAAAAAAAAAAABXilgAAAAAAAAAAAny7AAAAAAAAAAAAAAAAAAAAAAAAwnNjCAAAAAAAAAAAAdAJUAAAAAAAAAAAAAAAAAAAAAAAqhVQAAAAAAAAAAAASp0AAAAAAAAAAAAAAAAAAAAAAAHw5ma4AAAAAAAAAAAAB0c3gAAAAAAAAAAAAAAAAAAAAACLOfAAAAAAAAAAAAAAtZagAAAAAAAAAAAAAAAAAAAAACrFTAAAAAAAAAAAAABLl+AAAAAAAAAAAAAAAAAAAAAAKOQQAAAAAAAAAAAAANg6aAAAAAAAAAAAAAAAAAAAAAAc/IoAAAAAAAAAAAAAA6ofQAAAAAAAAAAAAAAAAAAAAAc7I4AAAAAAAAAAAAAA6iZAAAAAAAAAAAAAAAAAAAAAAc9IwAAAAAAAAAAAAAA6megAAAAAAAAAAAAAAAAAAAAAUMhgAAAAAAAAAAAAAZDqIAAAAAAAAAAAAAAAAAAAAABUCsgAAAAAAAAAAAAAkDooAAAAAAAAAAAAAAAAAAAAABBFHAAAAAAAAAAAAABYy5AAAAAAAAAAAAAAAAAAAAAAGE5keQAAAAAAAAAAAAC+EyAAAAAAAAAAAAAAAAAAAAAACjEGAAAAAAAAAAAADYOmH0AAAAAAAAAAAAAAAAAAAAAAEcc7AAAAAAAAAAAABbyzAAAAAAAAAAAAAAAAAAAAAAAApRXwAAAAAAAAAAAbp0Y9AAAAAAAAAAAAAAAAAAAAAAAAxHOjUAAAAAAAAAAB6OgkkAAAAAAAAAAAAAAAAAAAAAAAADQOfGIAAAAAAAAAAupYAAAAAAAAAAAAAAAAAAAAAAAAAARhQjEAAAAAAAAD6XAsgAAAAAAAAAAAAAAAAAAAAAAAAAANEoppAAAAAAAAyl1JsAAAAAAAAAAAAAAAAAAAAAAHkhyINAwGcthKAGMqZXDyAAAAAATJcTbANUqJHmY3SVJsygAAAAAAAAAAAAAAAAHwrhVjXAABYi3GUA1CtECYAAAAeyZLKSgB8K2VMxgAHssRajMAAAAAAAAAAAAAAADXKKRgAAABslyJoAHwjyNNQwgyG0SBJnsAGgUsjQAAADZLySYAAAAAAAAAAAAAAMBz00wAAAAATBaiTAAAAAABqlXK8eQAAAADIX0lAAAAAAAAAAAAAAfDn5FgAAAAAAEiT5Nm0AAADEQxAkOfAAAAAAAZzopsgAAAAAAAAAAAAFXKkAAAAAAAAAbZIm2Zj6YjVNA0D4AAAAAAAACYL6AAAAAAAAAAAADXObGMAAAAAAAAAAAAAAAAAAAAAAAvZNAAAAAAAAAAAAFPK0AAAAAAAAAAAAAAAAAAAAAAACQOigAAAAAAAAAAAxnMjGAAAAAAAAAAAAAAAAAAAAAAAAX8lgAAAAAAAAAACvFLAAAAAAAAAAAAAAAAAAAAAAAABPl2AAAAAAAAAAAKGQwAAAAAAAAAAAAAAAAAAAAAAAAMp08+gAAAAAAAAAHk5eeAAAAAAAAAAAAAAAAAAAAAAAAADoJKAAAAAAAAAAEYc9AAAAAAAAAAAAAAAAAAAAAAAAABbC0gAAAAAAAAAFaKeAAAAAAAAAAAAAAAAAAAAAAAAACaL2AAAAAAAAAAUor4AAAAAAAAAAAAAAAAAAAAAAAAANo6WAAAAAAAAAAc8I0AAAAAAAAAAAAAAAAAAAAAAAAAA6kewAAAAAAAAAcwMIAAAAAAAAAAAAAAAAAAAAAAAAAB0Y3wAAAAAAAADwctAAAAAAAAAAAAAAAAAAAAAAAAAABfSYAAAAAAAAANU5oAAAAAAAAAAAAAAAAAAAAAAAAAAC7E+AAAAAAAAAaBzkAAAAAAAAAAAAAAAAAAAAAAAAAAFxLIAAAAAAAAARpzwAAAAAAAAAAAAAAAAAAAAAAAAAAFtLQAAAAAAAAARZz4AAAAAAAAAAAAAAAAAAAAAAAAAAFrLUAAAAAAAAARRz8AAAAAAAAAAAAAAAAAAAAAAAAAAFrLUAAAAAAAAARxzsAAAAAAAAAAAAAAAAAAAAAAAAAAFvLMAAAAAAAAAYDmIAAAAAAAAAAAAAAAAAAAAAAAAAALwToAAAAAAAAAOaGqAAAAAAAAAAAAAAAAAAAAAAAAAAdKNsAAAAAAAAAFPK0AAAAAAAAAAAAAAAAAAAAAAAAADfOjAAAAAAAAAAGmc4PIAAAAAAAAAAAAAAAAAAAAAAAABdSwAAAAAAAAAAAqZVgAAAAAAAAAAAAAAAAAAAAAAAASh0EAAAAAAAAAAA+FGIQAAAAAAAAAAAAAAAAAAAAAAAG6dANgAAAAAAAAAAAHwqRWT4AAAAAAAAAAAAAAAAAAAAAATBdjOAAAAAAAAAAAAAR5ViGPAAAAAAAAAAAAAAAAAAAABKFmJsAAAAAAAAAAAAAAA8EaaRgPgAAAAAAAAAAAAAAAABlNwkjOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/xAAuEAAABQQBAwMCBgMAAAAAAAACAwQFBgABQFAWEhMUERUwIDEQISIjNZAkNGD/2gAIAQEAAQUC/ovMPCTQ3xOXV5MmtXKE16DIE46KWln/APBrn8lvpXNhCpQ+Hqqvf1v9KdyNS0lmRxVIZWQrq1+q28dH0pqs5SU5w+VA7mtomqVlrd0IXQF6lvVQhdV/nZpKNtpIsAuJ2x54UxT7IROosJtcxtZ7W6gdU+0ONsQU+vl3Y7EQLxNyhrcwuqbZyZ89wOxml0E1K0ykKsjYyx48QjIirx4anYLFVkSZWqutU5MbdPcm/XzRw/PKjbh7e564QugK9X5y3LZFvntmtkyrxWfMhCr8tbOD/wBOZFj+w9a2Ym9btmN5vZXa2Ti6nzM+1Bv1B1kh/ms1N/r6ySW6XvNKt0layWl9D1mJy+6frZuT6KsxgJ77xrZim7zVmQpN1rtasT+WkGDtjy4mj8Zp10tQeK5ZTejuvWAB2ga5/bfc277ZUNbOgvYSxp8RVkNLbd0WlF2JL2CtKFanc24TYrxiy7mjYmizSk2Ty0Bd0ylMJIdixlg8IO0e2MLuSqSiRnYVret45G/G27q0AdiXNpMajcBOmErNYo0Ft15hligq5aQmpRNjBUZKFJlCezxUjkp6QbVIi3T6D04VRTtDxFUMFyxfK1Rg1wpvay2wr8XB0LbC3GWmqr2eT7UXJFJdETM4ukszKNpMsAsBoxC6AukxCTSxwMXj+n7U0S0SakyoKwr8V7SU5BXQsZdKUJiMXwFFXPEhiJymm6OEt30CF02d5fYqjzxKTPpKOEQNsmIiqSqwLStAuXgbk7u/Ddh/ChcBtxrRKC3D6RB6rKI8nU0dCSxUODitV4SdXCjqDCB0VBw2pPFk5FFEBID9Dm9FtQHV+Mdb/ChcBtxzK+gdy85ctC3pnR0E6qfkbJOa303yIlw+da6FNwXKYjPoY7mC+Mk66Y1ie7O5GYIXQF+ebuyr50L+cgpHNQjpK6lLfhUKwJbLJiSRS2VHq6ELqv8AMjWCQKW5eFySZcwd+m2GndjktEzBQXQJwK1WnNq5xahzm9GzM4dHvyhTQhdV8KNu/tq3KcFtm9GeddSdr4s5+cgyZm49xRsGNw9tcchUoslTqDrqT9jGF3mtWPM1nZQ7KHLOw448sV+S77JKf4qkA+4DFMM7RZ5vfO2caU+SzYslUeOzbSDqP0Ys3P8ARNtIif2njFmh3W57RqO8dyxZGb3nrafaiDO8RiLTO8s2rEZ3WfDOH2ydtEx9TJhuguhs20LF6tWG/X6WfbQi/wDh4ckv6Mm2g9/2sOT/AMFtoN9sOUfwW2guJIg9bLtoOH9jDXleQh20OK7bTiOibw3DatKbw23EmiLtqdoxovPc8V3Qe5IBg7YtnEG3xkmNLmb0FsmBn91V2t02xhB6wyGP3bTNg1tY3VQhQhbk2QIPWF6idy73t031rPHBud0aICAjLcmIp0pfETktGFXJFqPvSKNnrabYoUi0BxAVAT4unPoyElXq8GrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFrgtcFq0GoEIBaiYinKpOgLSf0P/wD/xAAUEQEAAAAAAAAAAAAAAAAAAADA/9oACAEDAQE/AS/P/8QAFBEBAAAAAAAAAAAAAAAAAAAAwP/aAAgBAgEBPwEvz//EADwQAAEBAgoGBwgCAwEAAAAAAAECAAMREiEiMTNAUFGiBCNBYXHhEyQwMkJSgQUgYnKRobHBktFggpDw/9oACAEBAAY/Av8AhfrlBPEtOeo+rVv2LVuUtI9S2oWhXA/4HrlQnBMrdSQE71StrXivSRpfe6u8WPVushK/s2sPRn4mm37rTCryhoAYicE9r1VUmGxoulatX2vqFcgYo9lfz/pp1giv57vDBo2imEXuVPzAA0V1Nd4Y2ONox4jFozinaML1KnxgApabI7FAsoXoxl/LRnPqML06PRjq0/c2eM7o8QxYLcGEKvLotHM5dO4Wno35mLo3G8VLe0JDKW+pUbVrO+iQ3gly7+ZVrEburmm74VbGWtXiNsQo0wQHjdy4KVTba8dn5hdzpA3qtqPim3dB5Ui2u1YKBu556fj/AAt7xtyeF2veP6tw4Xar4gDbUjEwXc7VimD/AN9ba6HxQ3dGHgVDbVrPhTBdykK8QgYhdItgKqXk674yaHkvra0oR4iwCKBJd5Ce8JUtLai+e+KRN49K57q6dxtIQmjacAwS6kAkF4lD+hTFD70ONnAdyktAe+rvG84FSKHdLFOkCAizdLpg1hoGF64LFBYp0kQEWORg99oCd4U4XvA+kIoVg0GkiTYdhsITo4KiWj6TOefi74XpAG9tWS8Pwt1ZCU8ZWrIOAaV88+rSrKxgqVoO6vyn3IukAKBaN7MnDy7WgeCA9tC9mIxLQaKOJ2n3IdKPAbS0GiatP3auefyaR6fVtcEKbrSVI+7Q6KoK4XJCuQNF9miMfMaGh0tRV70jRfaM9OO0NG0YhQ9zrSYd+1odAVGGBpbrSFJ49jA5BUdzdYgdjfS0IEZWKvcnNF9lzj5tjRn5Kidp96FwSk4hoPaQjDzClo2iqChcMfSjAPy06ajYnso2iKg/bRdImL+x92c092BwkbULWOMrat6PUNNW7+7SqdfUtPeJ+ja96o8BA3cjfMWgcJCRuHu68ztiRS0+ajyjso2imD9t5VilNvK9IoDRn1GwYdrAvWIwLTVRVYK7frawN21oPZwiDE0tC8MJPaBTgwENOkeJ7wtsK5AGm1ae6LBqVwjBUrdeQU7w3VniTu7HrKkp4lurAvD9A2rPRj4WnduF6PSGC3O2ndbOgcbZV2TUPFj1afEVxDax0DwLSuT/ACaR0f5NMc5m1YQn0bWPVekjTrHA97i5DutalvNjFT2lRhN4RXnfdyelqDp3QmU8bxSo90yK4WlS3lCRCylPaVGG8hG7yJptCXafGfteZQqh4PvaCBQgRbzStPhMLAooNmJXQJWUpdKjDeiIfDNszyDbNvV4g/NZnaMTDeoHnBFmSnypvV0rBQsz3jBesjJViIbKtWKib2dfLBZCcBe6dxNke/Ifxe6ty/6sj35b3efNZHvC93vEWR76fm933+v7sjz0/N7vv9f3ZHvC93p3iyPE4pIveHzKJsrxGCr2dpOxMtlS9TQqQ8b1QnZSeFmUjbs4tAukXp0j2l5Rws/T6MPn/u859WnvNNs8C6C0fRpXRy3jFcUbTgwRo9AtMC5QWK/Zco8jS3dC8mO8cWi6KIBbNcIFeYN1bWDdS0D0EHfdMjSJijFUjQ6RrFb6LggfpChvDdyLwLapaw019lauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2rsnNq7Jzauyc2lfZObax4o8A04KVxLdWQlPAf8AB/8A/8QAKxABAAEBBgUEAwEBAQAAAAAAAREAITFAQVBRYXGBkaGx0eHwIDDB8ZBg/9oACAEBAAE/If8AhebPIgpzw5elXuPtwouX08q9a9nrXiUX/wAG2gjxnxTaHbku1N3fZQ8Uk1t/K5x2LHanYB5S8e1QI2zc70ZqEbk12EbHeddqRZHnern+22SZnaulKkK7tvrlrRogFquVMkQXOZ+s6R1qtquAWJNxfye1Eyv41cGx5VpDKLZm4nB9XhuFSJgX961UcB5TlUPm3n8XjhYOAvMhs0Fs27PWqZ4i1PtGHBW6s2VROlI6lkgrZ9bcTn0b31t1HMrHPhTV3xxUFKep7OoSofSwxdqcfEvfTzRYBK02YxOBl4xk357Ss062SLH1v8TjZl9i8P8ANOhzBT0P7jbcXFXb3DTovYvV/uN+mY6dybDwxoysqPTMnTXP13Y5TLt9NNtVsfDHRlyBpsw/xI/mN4G/lp3Hu7XGuHnht/mnQF6XQ2e2NyIROb/jpx3VejG4SE44yCVp2ZfeOn2V2fue/XF7Dwuxm9qMXAgOGnwH9e260ksUi5b2WbqNmFv9PHEzal6oFiHBsaiBslDWQjbsN8ODaiAM2rJrafw6anuHGy+1I0fCOFCWyoewFtl99Vygf43hSdb8ODSK2tc2FPN46vHnJV/wq8MXOBLquQVAw5O3Lx46ejAr1QUwnCCzu0rZ3Er+UzkbCfyvQgyptmBfVfRQHjl/Jz/BheSGkVc2dzlvSFAWIkJ+5ELhlryKsl5vfI/hYHzceAp30K919qufuqvGcg+pSlm8oadhHe57+Kgh+K7RDZALVWwpBF9gb1IiZDcdPyGVI6a5PumgqzmfhADWRsHWmEXK+670qJcgWd/0y1jIS1Cvc3ZTBD+N2MvwB1gFqtTcVyu5y3pXeyT8iZTckNLHTQOmdG0HM0FlALjNbFRRQtg+u/6peHmZcxUBDtr2H8QjIjk07K2/tV6Er2Vf15pRLzzfZR90eKfd+Sa8XH7qvdPeTxdXCQkH430uYPao+9i667/qk7WZlzFQEsXyHDHrjHcXYps8CzLH7SR4M205NRZNcJ+f3zhDK8ulEPWN8aRotCrK/seg+RKuqB8pwxpugCVcir4yw/pgIMQeE+KjhDmnajrRd0PZt/TLxeRUocKeo1OgbN7vSOtVvX97DQ/fhV2vcbtsZBrbaDbIwlyoZWjs0Db+rlFeepPevon0p+7elILI5z/leG5L61dgNvapJ2q5uDto8yycXdLmw3cilnliahb3No45HFSW9XXePXUZ9epPs0MlmIvGQqtQmLUrW58Nd4xEqLZnk+Y1OVVw6LT+4iQtiHO99dTe/p0JBISPDDFdJVypb9BddU36Eul3iMNJrxB1+J1WTJkHo+hhoqz+0+dV+mLb/MNBFw91fjVeYi5TbhpBt4CNVGVIwy+4wv1aHVp7wdln8wnGsdXiu18z/cJOTL12rxxzfTCOqxq84bB8YToB6mr2X9bcI4+i5q7wmW/ru6vcwkhRx9kdXj3heH3wkaZZ21e3X+YfzC5KDRyy8atG6EOptfOFjbY9J8emqz4SXpvsYYX7xL2yUiiiQjk6pPl6T54fJg3DLUzHBvVvwoxBAWAYc0AQhHOlQKP8PDUY2QL+4UUMZma7uJNxAQjnRot+5jlvToGEvHTj025LeSiRT7ru4w6ejr80gsOm7KirWQh0kJUZFfuovpws/ZY6UEXY/gxSals7djxdXlGho28Ofz0NmZmZmZmZmZmZmZmZmZmZmZhpPHcFfS35RXf+tO//AAf/AP/aAAwDAQACAAMAAAAQkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkEEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgEEkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgEkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkgEkkkkkkkkkkkkkkkkkkkkkkkgEkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkgEkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkgEkkkkkkkkkkkkkkkkkkkkkkkkgAkkgkkkkkkgkkEAAEkkkkkkkkkkkkkkkkkgkkkkkkAEkkkgkkkAkkkEkkkkkkkkkkkkkkkkkkkkkgEkkEkEgkkkkkkkkEkkkkkkkkkkkkkkkEkkkkkkEkkkkkkkEEkkkkkEkkkkkkkkkkkkkkkkkkkkkkgAkkkkAEkkkkkkkAkkkkkkkkkkkkkgkkkkkkkkkkEAkEkkkkkkkkkEkkkkkkkkkkkkgEkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkAkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkAkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkAkkkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkEEkkkkkkkkkkkkkkkkkkkkgkkkkkkkkkkkkkkkgAgAAAAAAAAAAAAAAAAEgEkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkn//EABQRAQAAAAAAAAAAAAAAAAAAAMD/2gAIAQMBAT8QL8//xAAUEQEAAAAAAAAAAAAAAAAAAADA/9oACAECAQE/EC/P/8QAKxABAAEBBgUDBQEBAAAAAAAAAREhADFBUWGBQFBxkaGxwfAgMNHh8WCQ/9oACAEBAAE/EP8AhfF3Z+s2hCoy/es54r+ljVDcLWSBOd9IsuCjh6a/4PrRWnJw3JbAyDcEIDzZhVP16NnCKXqyv1KEIxv4rEh+9Ze9FiCu0W6GneLGGaUSJz1zTcrHrwDrsNnF0Uwk7vgafdPMln401IbBLRhqOnoPdsXc5DiKkAC9VuLPWOVnYYGquUX2To6hKriuLwCPHpJJ631U6WxTil6yTB0ebzmaNQPd0xtiweYUu/Dca8GQpGCsyye95YtSAV0zmZOPNVXEagBbG1aYV8mhTNeEuKVTqAxLOfBUM5N0yceZrBWypiOKFjqMM78o4aae/SmT1ybAqhd6OSNE5kwxJqKpSNG50nM4lgjRUqNc9LjtrzFXYc81gNVgtPgVlGQaBBxWDZy3/knkeYPR0iHP+z24tV4o2lfQhtPLw8KMwC9tNlJp6A2gcY26Qrw7xO/LpbSGr+rxrBA5cDkVcHpHqcagCjeSRy4VkpG6r7DjanRLbHl03wR7Hxri0pWx3HLlr4Jg46TXvbctcOLuk9+Oe/XiP8VaQxn8kDl1CaJO9xqk0hLby6cFKJ3vl7ONjlO6P8dzl0OxI8lKOzZ8DbsAwnGUqGrPB4E8vKsSNyBQ+nFkx4gDq7ErFANLAEBy+Ox6vH2JO1nUCJRHDikjeaX5JSNnPmK3z6ApeO17rOnE1izGLi964GrYZIYbgIDmNFbpiZJkjU6WDabiIFuHyjw4ATHSpQCxkYI2eBcvyceZsY5cOuU6sf1b3/5EMxz4VCCqtAtloIX+es7FM+aya4kf9PR3FCjwC/UcRz4M2CiACVbSgG6E6PY4dbubUTlQ1vfMel9qjCU8ho4OjXgUZlRzu5GrYCk1QT77+GvLhqnIgdVoWLNaRedo7TZAXIHxKHhswvX7VV5snIPiUsbU2qB6KjZtNmipqu14On0BeKByOujrZh18Xurhpf1sqTZcjU+8ipq3b+NWnW0eCGIT4UKfRF2xivgX3WbGrSc9bD8S2YneP6tkDoXq5YoYuVtuMeLHEa8U9yLGcpUXcXm/JBpjBgDFW6xCHRJD0X+B1tUvkojpChsfU4qRGRGpag2UNQ1y+et1rveb3aJeOj9ECVI7AD0uteg8fsiyRZmBUXS47fZG3SntsWqTH8h90suaspIex6tfoBo8hAGa2hJcCnoMWt3WyHimQX9fVfoJMNy2GMYTqrng9bYpYG5yS8dHkICaAK4AMWzZkhRtFYvBh9oooaxZXDY2b6FQalzo+fpTUjISNnynm+MDxaXU3AAePVa4H+LC2uItFbhl0S35vx0tFameoNkAlcT4I8LQdDDHsfSuZYlU9QwavmyGvZTsli60yD7RDHuqWWIWoJlmf6ejsvHQD/QOizFtVpAT0Zm5uPY+7daAMgyvToyWBGophnIbtjP32EDSLsDWzjWpSQ0LvJ1LOc6XIzVv+5PNo0I2gMEQN/vPDsvGP/A9AF62ZLRblOb1fBwH6sOkmuxLYE+X35VPNg2FHst4fZovyaUuhjaADXI9w8WnFmjvde0WTs0olfv1scQ3DFZiWZiAiWuKvmXGNGICF17dvds+Eii7Cce4WEGoxvnZET3xZLMLHQNuPXUllVVm3s2mwLBAd4eLSg0xA+myasZSV4NijuSXew46cXDUy8zpuMWc3nAK8wj6MzX/AKqNteKwbGHEqNlzErUJB3HamywEsTiE036gusuUk9VnmUd3gJeeHZ4imFHei+rseZ04I4+JTiFRT4gMYbcznliesMxYOgeeVThmuhJ0EtvJiep5pNVLtOccMJwRrhOHmpKS4vXhglxextc11/gU7cNypvY1D05qrhB4Y8Hhq60M7Xs5q4qRKjYLo+bOFrVPnm82rPMcJN/EvOb/ABpXwhjag9ebmQI7TcJKf7Kc3/q6frwkn0eb3SSePx4TaHN/SNf8WGbzwm5S4fIZc3WLp2vhA4eXrRcc3dTuB0D3XCpGQE62fIc2aVmDL3g8K16DexPXmo3R4W8Z603cNHiTNg3vbotkuKvwGE5osOxxbwu7q9A4dUkRhr1x7HZz5nFEQu+g6vgnSwJBAEAFwcOnhAkgbxt1g4lP8Dt15gIpgm65zcjHvbGPpdUmK8S+kA8gcEs8QZr27S/rZCSIBCPLhvvLRGnHrd1utXPCxXEJi8Y4pinmcN1oQr0m/wBlsjv3NDZ5S5gVaAWfG9J7na2P4BibTHvNoEUBgcfot4HzZoyX4x6LSPx/kW9SZ5HRERERERERERERERERERERETFT0jZcxfBmbRq2Ztq+ByIvV/wf/9k='
}
