
'use strict';
import React, { Component } from 'react';
import { AppRegistry, AsyncStorage } from 'react-native';
import { Toast, Root} from 'native-base';

import axios from 'axios';
import Base64 from "../config/Base64";
import {showToastBottom, showToastTop} from '../utils/helpers';



export const apiURI = 'http://172.16.10.6:5000/api';

export const headers = {
  headers: {
  'Content-Type': 'application/json',
  'api-version': '1.0',
  }
}

export const headersAuth = {
  headers: {
    'Content-Type': 'application/json',
    'api-version': '1.0',
  }
};

export async function postCandidato(user) {
  return new Promise((resolve, reject) => {
    axios.post( apiURI + '/candidatos/',
    {
      nome: user.nome,
      sobrenome: user.sobrenome,
      email: user.email,
      senha: user.senha,
      celular: user.celular,
      cargo: user.cargo,
      descricao: user.descricao,
      endereco: user.endereco,
      habilidades: user.habilidades,
      valores: user.valores,
      imagem: user.imagem,
    },
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
    }})
    .then(res => {
      resolve(true);
    })
    .catch(error => {
      //showToastTop('Não foi possível realizar a operação. O servidor está em manutenção. Tente novamente mais tarde');
      reject(false);
    })
  });
}

export async function getVagas(token){
  return new Promise((resolve, reject) => {
    axios.get( apiURI + '/candidatos/vagas/',
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + token
    }})
    .then(res => {
      resolve(res.data);
      //let vagas = res.data
      //this.setState({vagasApi : vagas});
    })
    .catch(error => {
      reject(error);
      //console.warn(error.message);
    });
  });
}

export async function getCurtidas(token, idCandidato) {
  return new Promise((resolve, reject) => {
    axios.get(apiURI + '/curtidas/candidato/' + idCandidato,
      {headers: {
        'Content-Type': 'application/json',
        'api-version': '1.0',
        'Authorization': 'Bearer ' + token
    }})
    .then(res => {
      resolve(res.data);
      //this.setState({curtidas: curtidasApi});
    })
    .catch(error => {
      reject(error);
      //showToastTop("Não foi possível carregar as curtidas. O servidor está em manutenção. Tente novamente mais tarde.");
    });
  });
}

export async function getMatches(token, idCandidato) {
  return new Promise((resolve, reject) => {
    axios.get(apiURI + '/curtidas/candidato/' + idCandidato + '/matches',
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + token
    }})
    .then(res => {
      resolve(res.data);
    })
    .catch(error => {
      reject(error);
      //showToastTop("Não foi possível carregar os matches. O servidor está em manutenção. Tente novamente mais tarde.");
    });
  });
}

export async function getCandidato(token, idCandidato) {
  return new Promise((resolve, reject) => {
    axios.get( apiURI + '/candidatos/' + idCandidato,
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + token
    }})
    .then(res => {
      resolve(res.data);
    })
    .catch(error => {
      reject(error);
      //showToastTop('Não foi possível carregar o Perfil. Tente novamente mais tarde');
    });
  });
}

export async function curtir(vaga, token, idCandidato) {
  return new Promise((resolve, reject) => {
    axios.post( apiURI + '/curtidas/candidato',
    {
      "idVaga": vaga.id,
      "idCandidato": idCandidato,
      "candidatoCurtiu": true
    },
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + token
    }})
    .then(res => {
      resolve(true);
    })
    .catch(err => reject(err));
  });
}

export async function descurtir(vaga, token) {
  return new Promise((resolve, reject) => {
    axios.delete( apiURI + '/curtidas/candidato/vaga/' + vaga.id,
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + token
    }})
    .then(res => {
      //state.setState({curtiu: false});
      resolve(false);
    })
    .catch(error => {
      showToastTop('Não foi possível realizar a operação. O servidor está em manutenção. Tente novamente mais tarde');
      reject(error)
    });
  })
}

export async function updateCandidato(user, token, idCandidato) {
  return new Promise((resolve, reject) => {
    axios.put( apiURI + '/candidatos/' + idCandidato,
    {
      nome: user.nome,
      sobrenome: user.sobrenome,
      email: user.email,
      celular: user.celular,
      cargo: user.cargo,
      descricao: user.descricao,
      endereco: user.endereco,
      habilidades: user.habilidades,
      valores: user.valores,
      imagem: user.imagem64,
    },
    {
      headers: {
      'Content-Type': 'application/json',
      'api-version': '1.0',
      'Authorization': 'Bearer ' + token
    }})
    .then(res => {

      resolve(true);
    })
    .catch(error => {
      //console.warn(error.response);
      //showToastTop('Não foi possível realizar a operação. O servidor está em manutenção. Tente novamente mais tarde');
      reject(false);
    })
  });
}

export async function getHabilidades() {
  return new Promise((resolve, reject) => {
    axios.get(apiURI + '/habilidades',
      {headers: {
        'Content-Type': 'application/json',
        'api-version': '1.0',
    }})
    .then(res => {
      resolve(res.data);
    })
    .catch(error => {
      reject(error);
      //showToastTop("Não foi possível carregar as habilidades. O servidor está em manutenção. Tente novamente mais tarde.");
    });
  });
}

export async function getValores() {
  return new Promise((resolve, reject) => {
    axios.get(apiURI + '/valores',
      {headers: {
        'Content-Type': 'application/json',
        'api-version': '1.0',
    }})
    .then(res => {
      resolve(res.data);
    })
    .catch(error => {
      reject(error);
    });
  });
}

export async function getCargos() {
  return new Promise((resolve, reject) => {
    axios.get(apiURI + '/cargos',
      {headers: {
        'Content-Type': 'application/json',
        'api-version': '1.0',
    }})
    .then(res => {
      resolve(res.data);
    })
    .catch(error => {
      reject(error);
    });
  });
}
